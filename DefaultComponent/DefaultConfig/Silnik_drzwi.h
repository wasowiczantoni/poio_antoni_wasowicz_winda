/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Silnik_drzwi
//!	Generated Date	: Thu, 1, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Silnik_drzwi.h
*********************************************************************/

#ifndef Silnik_drzwi_H
#define Silnik_drzwi_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Silnik_drzwi
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Silnik_drzwi
class Silnik_drzwi : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSilnik_drzwi;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Silnik_drzwi(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Silnik_drzwi();
    
    ////    Operations    ////
    
    //## operation Otwieranie()
    void Otwieranie();
    
    //## operation Zamykanie()
    void Zamykanie();
    
    //## operation message_0()
    void message_0();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getOkres_drzwi() const;
    
    //## auto_generated
    void setOkres_drzwi(int p_okres_drzwi);
    
    //## auto_generated
    int getPredkosc_drzwi() const;
    
    //## auto_generated
    void setPredkosc_drzwi(int p_predkosc_drzwi);
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    Stany getKierunek_drzwi() const;
    
    //## auto_generated
    void setKierunek_drzwi(Stany p_kierunek_drzwi);
    
    ////    Attributes    ////

protected :

    Stany kierunek_drzwi;		//## attribute kierunek_drzwi
    
    int okres_drzwi;		//## attribute okres_drzwi
    
    int predkosc_drzwi;		//## attribute predkosc_drzwi
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // Waiting:
    //## statechart_method
    inline bool Waiting_IN() const;
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    // Open_doors:
    //## statechart_method
    inline bool Open_doors_IN() const;
    
    // Close_doors:
    //## statechart_method
    inline bool Close_doors_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Silnik_drzwi_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        Waiting = 2,
        sendaction_6 = 3,
        sendaction_5 = 4,
        Open_doors = 5,
        Close_doors = 6,
        Aktywny = 7
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSilnik_drzwi : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Silnik_drzwi, OMAnimatedSilnik_drzwi)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Waiting_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Open_doors_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Close_doors_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Silnik_drzwi::rootState_IN() const {
    return true;
}

inline bool Silnik_drzwi::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Silnik_drzwi::Waiting_IN() const {
    return rootState_subState == Waiting;
}

inline bool Silnik_drzwi::sendaction_6_IN() const {
    return rootState_subState == sendaction_6;
}

inline bool Silnik_drzwi::sendaction_5_IN() const {
    return rootState_subState == sendaction_5;
}

inline bool Silnik_drzwi::Open_doors_IN() const {
    return rootState_subState == Open_doors;
}

inline bool Silnik_drzwi::Close_doors_IN() const {
    return rootState_subState == Close_doors;
}

inline bool Silnik_drzwi::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Silnik_drzwi.h
*********************************************************************/
