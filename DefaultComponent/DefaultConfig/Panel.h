/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Panel
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Panel.h
*********************************************************************/

#ifndef Panel_H
#define Panel_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Panel
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Panel
class Panel : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPanel;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Panel(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Panel();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPanel : public OMAnimatedModul {
    DECLARE_META(Panel, OMAnimatedPanel)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Panel.h
*********************************************************************/
