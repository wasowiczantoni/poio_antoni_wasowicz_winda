/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_1
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/actor_1.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "actor_1.h"
//#[ ignore
#define Default_actor_1_actor_1_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor actor_1
actor_1::actor_1() {
    NOTIFY_CONSTRUCTOR(actor_1, actor_1(), 0, Default_actor_1_actor_1_SERIALIZE);
}

actor_1::~actor_1() {
    NOTIFY_DESTRUCTOR(~actor_1, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(actor_1, Default, Default, false, OMAnimatedactor_1)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/actor_1.cpp
*********************************************************************/
