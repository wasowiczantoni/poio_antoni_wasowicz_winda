/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Wed, 31, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include <oxf/event.h>
//## auto_generated
class Czujnikpieter;

//## auto_generated
class Detektor_kolizji;

//## auto_generated
class Glosnik;

//## classInstance itsKontroler_Windy
class Kontroler_Windy;

//## auto_generated
class Modul;

//## auto_generated
class Panel;

//## auto_generated
class Przycisk;

//## auto_generated
class SilnikWindy;

//## auto_generated
class Silnik_drzwi;

//## auto_generated
class Wyswietlacz;

//#[ ignore
#define evWezwanieWindy_Default_id 18601

#define ev_na_ktore_pietro_jechac_Default_id 18602

#define jedz_w_gore_Default_id 18603

#define info_ktore_pietro_Default_id 18604

#define evZmianaPietraDOWN_Default_id 18605

#define jedz_w_dol_Default_id 18606

#define eventmessage_0_Default_id 18607

#define evOtrworzDrzwi_Default_id 18608

#define Drzwi_otwarte_Default_id 18609

#define evZamknijDrzwi_Default_id 18610

#define evSprawdzKolizje_Default_id 18611

#define evKolizja_Default_id 18612

#define evWyborPietra_Default_id 18613

#define evWlaczenie_Konserwacji_Default_id 18614

#define Potrzebna_weryfikacja_Default_id 18615

#define podanie_loginu_Default_id 18616

#define Sprawdzenie_poprawnosci_Default_id 18617

#define login_poprawny_Default_id 18618

#define Brak_autoryzacji_Default_id 18619

#define wybor_trybu_Default_id 18620

#define co_sprawdzic_Default_id 18621

#define Sprawdzamy_Default_id 18622

#define evAktywuj_Default_id 18623

#define evStart_Default_id 18624

#define evStop_Default_id 18625

#define ev_zabl_czuj_odb_Default_id 18626

#define evPrzycisk_Default_id 18627

#define evKrok_Default_id 18628

#define evKrok_drzwi_Default_id 18629

#define info_o_pietrze_Default_id 18630

#define evWezwanie0_Default_id 18631

#define evWezwanie1_Default_id 18632

#define evWezwanie2_Default_id 18633

#define evWezwanie3_Default_id 18634

#define evWezwanie4_Default_id 18635

#define evWezwanieGaraz_Default_id 18636

#define ev1_Default_id 18637

#define ev2_Default_id 18638

#define ev3_Default_id 18639

#define ev4_Default_id 18640

#define ev0_Default_id 18641

#define ev0Garaz_Default_id 18642

#define evOK_Default_id 18643

#define evpozycja_koncowa_Default_id 18644

#define pietro_wybrane_Default_id 18645

#define evS_Default_id 18646

#define evuruchomienie_Default_id 18647

#define evZmianaPietraUP_Default_id 18648
//#]

//## package Default


//## type Stany
enum Stany {
    TESTOWANIE = -3,
    ROZRUCH = -2,
    ZAMYKANIE = -1,
    OTWIERANIE = 1,
    DOL = -1,
    GORA = 1,
    ZAMKNIETE = -1,
    OTWARTE = 1,
    ZATRZYMANIE = 0,
    WYBOR,
    WEZWANIE
};

//## type PIETRO
enum PIETRO {
    PIERWSZE = 1,
    DRUGIE = 2,
    TRZECIE = 3,
    CZWARTE = 4,
    PARTER = 0,
    GARAZ = -1
};

//## classInstance itsKontroler_Windy
extern Kontroler_Windy itsKontroler_Windy;

//## auto_generated
void Default_initRelations();

//## auto_generated
bool Default_startBehavior();

//#[ ignore
class Default_OMInitializer {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Default_OMInitializer();
    
    //## auto_generated
    ~Default_OMInitializer();
};
//#]

//## event evWezwanieWindy()
class evWezwanieWindy : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanieWindy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanieWindy();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanieWindy : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanieWindy)
};
//#]
#endif // _OMINSTRUMENT

//## event ev_na_ktore_pietro_jechac()
class ev_na_ktore_pietro_jechac : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev_na_ktore_pietro_jechac;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev_na_ktore_pietro_jechac();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev_na_ktore_pietro_jechac : virtual public AOMEvent {
    DECLARE_META_EVENT(ev_na_ktore_pietro_jechac)
};
//#]
#endif // _OMINSTRUMENT

//## event jedz_w_gore()
class jedz_w_gore : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedjedz_w_gore;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    jedz_w_gore();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedjedz_w_gore : virtual public AOMEvent {
    DECLARE_META_EVENT(jedz_w_gore)
};
//#]
#endif // _OMINSTRUMENT

//## event info_ktore_pietro(PIETRO)
class info_ktore_pietro : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedinfo_ktore_pietro;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
//#[ ignore
    info_ktore_pietro(int p_pietro);
//#]

    //## auto_generated
    info_ktore_pietro();
    
    //## auto_generated
    info_ktore_pietro(PIETRO p_pietro);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    PIETRO pietro;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedinfo_ktore_pietro : virtual public AOMEvent {
    DECLARE_META_EVENT(info_ktore_pietro)
};
//#]
#endif // _OMINSTRUMENT

//## event evZmianaPietraDOWN()
class evZmianaPietraDOWN : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZmianaPietraDOWN;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZmianaPietraDOWN();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZmianaPietraDOWN : virtual public AOMEvent {
    DECLARE_META_EVENT(evZmianaPietraDOWN)
};
//#]
#endif // _OMINSTRUMENT

//## event jedz_w_dol()
class jedz_w_dol : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedjedz_w_dol;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    jedz_w_dol();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedjedz_w_dol : virtual public AOMEvent {
    DECLARE_META_EVENT(jedz_w_dol)
};
//#]
#endif // _OMINSTRUMENT

//## event eventmessage_0()
class eventmessage_0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedeventmessage_0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    eventmessage_0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedeventmessage_0 : virtual public AOMEvent {
    DECLARE_META_EVENT(eventmessage_0)
};
//#]
#endif // _OMINSTRUMENT

//## event evOtrworzDrzwi()
class evOtrworzDrzwi : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOtrworzDrzwi;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOtrworzDrzwi();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOtrworzDrzwi : virtual public AOMEvent {
    DECLARE_META_EVENT(evOtrworzDrzwi)
};
//#]
#endif // _OMINSTRUMENT

//## event Drzwi_otwarte()
class Drzwi_otwarte : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDrzwi_otwarte;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Drzwi_otwarte();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDrzwi_otwarte : virtual public AOMEvent {
    DECLARE_META_EVENT(Drzwi_otwarte)
};
//#]
#endif // _OMINSTRUMENT

//## event evZamknijDrzwi()
class evZamknijDrzwi : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZamknijDrzwi;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZamknijDrzwi();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZamknijDrzwi : virtual public AOMEvent {
    DECLARE_META_EVENT(evZamknijDrzwi)
};
//#]
#endif // _OMINSTRUMENT

//## event evSprawdzKolizje()
class evSprawdzKolizje : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevSprawdzKolizje;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evSprawdzKolizje();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevSprawdzKolizje : virtual public AOMEvent {
    DECLARE_META_EVENT(evSprawdzKolizje)
};
//#]
#endif // _OMINSTRUMENT

//## event evKolizja()
class evKolizja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKolizja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKolizja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKolizja : virtual public AOMEvent {
    DECLARE_META_EVENT(evKolizja)
};
//#]
#endif // _OMINSTRUMENT

//## event evWyborPietra()
class evWyborPietra : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWyborPietra;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWyborPietra();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWyborPietra : virtual public AOMEvent {
    DECLARE_META_EVENT(evWyborPietra)
};
//#]
#endif // _OMINSTRUMENT

//## event evWlaczenie_Konserwacji()
class evWlaczenie_Konserwacji : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWlaczenie_Konserwacji;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWlaczenie_Konserwacji();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWlaczenie_Konserwacji : virtual public AOMEvent {
    DECLARE_META_EVENT(evWlaczenie_Konserwacji)
};
//#]
#endif // _OMINSTRUMENT

//## event Potrzebna_weryfikacja()
class Potrzebna_weryfikacja : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPotrzebna_weryfikacja;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Potrzebna_weryfikacja();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPotrzebna_weryfikacja : virtual public AOMEvent {
    DECLARE_META_EVENT(Potrzebna_weryfikacja)
};
//#]
#endif // _OMINSTRUMENT

//## event podanie_loginu()
class podanie_loginu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedpodanie_loginu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    podanie_loginu();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedpodanie_loginu : virtual public AOMEvent {
    DECLARE_META_EVENT(podanie_loginu)
};
//#]
#endif // _OMINSTRUMENT

//## event Sprawdzenie_poprawnosci()
class Sprawdzenie_poprawnosci : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSprawdzenie_poprawnosci;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sprawdzenie_poprawnosci();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSprawdzenie_poprawnosci : virtual public AOMEvent {
    DECLARE_META_EVENT(Sprawdzenie_poprawnosci)
};
//#]
#endif // _OMINSTRUMENT

//## event login_poprawny()
class login_poprawny : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedlogin_poprawny;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    login_poprawny();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedlogin_poprawny : virtual public AOMEvent {
    DECLARE_META_EVENT(login_poprawny)
};
//#]
#endif // _OMINSTRUMENT

//## event Brak_autoryzacji()
class Brak_autoryzacji : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedBrak_autoryzacji;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Brak_autoryzacji();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedBrak_autoryzacji : virtual public AOMEvent {
    DECLARE_META_EVENT(Brak_autoryzacji)
};
//#]
#endif // _OMINSTRUMENT

//## event wybor_trybu()
class wybor_trybu : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedwybor_trybu;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    wybor_trybu();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedwybor_trybu : virtual public AOMEvent {
    DECLARE_META_EVENT(wybor_trybu)
};
//#]
#endif // _OMINSTRUMENT

//## event co_sprawdzic()
class co_sprawdzic : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedco_sprawdzic;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    co_sprawdzic();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedco_sprawdzic : virtual public AOMEvent {
    DECLARE_META_EVENT(co_sprawdzic)
};
//#]
#endif // _OMINSTRUMENT

//## event Sprawdzamy()
class Sprawdzamy : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSprawdzamy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Sprawdzamy();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSprawdzamy : virtual public AOMEvent {
    DECLARE_META_EVENT(Sprawdzamy)
};
//#]
#endif // _OMINSTRUMENT

//## event evAktywuj()
class evAktywuj : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevAktywuj;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evAktywuj();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevAktywuj : virtual public AOMEvent {
    DECLARE_META_EVENT(evAktywuj)
};
//#]
#endif // _OMINSTRUMENT

//## event evStart(Stany)
class evStart : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStart;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
//#[ ignore
    evStart(int p_stan);
//#]

    //## auto_generated
    evStart();
    
    //## auto_generated
    evStart(Stany p_stan);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    Stany stan;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStart : virtual public AOMEvent {
    DECLARE_META_EVENT(evStart)
};
//#]
#endif // _OMINSTRUMENT

//## event evStop()
class evStop : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevStop;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evStop();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevStop : virtual public AOMEvent {
    DECLARE_META_EVENT(evStop)
};
//#]
#endif // _OMINSTRUMENT

//## event ev_zabl_czuj_odb()
class ev_zabl_czuj_odb : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev_zabl_czuj_odb;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev_zabl_czuj_odb();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev_zabl_czuj_odb : virtual public AOMEvent {
    DECLARE_META_EVENT(ev_zabl_czuj_odb)
};
//#]
#endif // _OMINSTRUMENT

//## event evPrzycisk(int)
class evPrzycisk : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevPrzycisk;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evPrzycisk();
    
    //## auto_generated
    evPrzycisk(int p_Pietro);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    int Pietro;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevPrzycisk : virtual public AOMEvent {
    DECLARE_META_EVENT(evPrzycisk)
};
//#]
#endif // _OMINSTRUMENT

//## event evKrok(double)
class evKrok : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKrok;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKrok();
    
    //## auto_generated
    evKrok(double p_krok);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    double krok;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKrok : virtual public AOMEvent {
    DECLARE_META_EVENT(evKrok)
};
//#]
#endif // _OMINSTRUMENT

//## event evKrok_drzwi(double)
class evKrok_drzwi : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevKrok_drzwi;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evKrok_drzwi();
    
    //## auto_generated
    evKrok_drzwi(double p_krok_drzwi);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    double krok_drzwi;		//## auto_generated
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevKrok_drzwi : virtual public AOMEvent {
    DECLARE_META_EVENT(evKrok_drzwi)
};
//#]
#endif // _OMINSTRUMENT

//## event info_o_pietrze()
class info_o_pietrze : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedinfo_o_pietrze;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    info_o_pietrze();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedinfo_o_pietrze : virtual public AOMEvent {
    DECLARE_META_EVENT(info_o_pietrze)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanie0()
class evWezwanie0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanie0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanie0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanie0 : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanie0)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanie1()
class evWezwanie1 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanie1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanie1();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanie1 : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanie1)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanie2()
class evWezwanie2 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanie2;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanie2();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanie2 : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanie2)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanie3()
class evWezwanie3 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanie3;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanie3();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanie3 : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanie3)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanie4()
class evWezwanie4 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanie4;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanie4();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanie4 : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanie4)
};
//#]
#endif // _OMINSTRUMENT

//## event evWezwanieGaraz()
class evWezwanieGaraz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevWezwanieGaraz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evWezwanieGaraz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevWezwanieGaraz : virtual public AOMEvent {
    DECLARE_META_EVENT(evWezwanieGaraz)
};
//#]
#endif // _OMINSTRUMENT

//## event ev1()
class ev1 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev1();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev1 : virtual public AOMEvent {
    DECLARE_META_EVENT(ev1)
};
//#]
#endif // _OMINSTRUMENT

//## event ev2()
class ev2 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev2;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev2();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev2 : virtual public AOMEvent {
    DECLARE_META_EVENT(ev2)
};
//#]
#endif // _OMINSTRUMENT

//## event ev3()
class ev3 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev3;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev3();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev3 : virtual public AOMEvent {
    DECLARE_META_EVENT(ev3)
};
//#]
#endif // _OMINSTRUMENT

//## event ev4()
class ev4 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev4;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev4();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev4 : virtual public AOMEvent {
    DECLARE_META_EVENT(ev4)
};
//#]
#endif // _OMINSTRUMENT

//## event ev0()
class ev0 : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev0;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev0();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev0 : virtual public AOMEvent {
    DECLARE_META_EVENT(ev0)
};
//#]
#endif // _OMINSTRUMENT

//## event ev0Garaz()
class ev0Garaz : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedev0Garaz;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    ev0Garaz();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedev0Garaz : virtual public AOMEvent {
    DECLARE_META_EVENT(ev0Garaz)
};
//#]
#endif // _OMINSTRUMENT

//## event evOK()
class evOK : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevOK;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evOK();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevOK : virtual public AOMEvent {
    DECLARE_META_EVENT(evOK)
};
//#]
#endif // _OMINSTRUMENT

//## event evpozycja_koncowa()
class evpozycja_koncowa : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevpozycja_koncowa;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evpozycja_koncowa();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevpozycja_koncowa : virtual public AOMEvent {
    DECLARE_META_EVENT(evpozycja_koncowa)
};
//#]
#endif // _OMINSTRUMENT

//## event pietro_wybrane()
class pietro_wybrane : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedpietro_wybrane;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    pietro_wybrane();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedpietro_wybrane : virtual public AOMEvent {
    DECLARE_META_EVENT(pietro_wybrane)
};
//#]
#endif // _OMINSTRUMENT

//## event evS()
class evS : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevS;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evS();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevS : virtual public AOMEvent {
    DECLARE_META_EVENT(evS)
};
//#]
#endif // _OMINSTRUMENT

//## event evuruchomienie()
class evuruchomienie : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevuruchomienie;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evuruchomienie();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevuruchomienie : virtual public AOMEvent {
    DECLARE_META_EVENT(evuruchomienie)
};
//#]
#endif // _OMINSTRUMENT

//## event evZmianaPietraUP()
class evZmianaPietraUP : public OMEvent {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedevZmianaPietraUP;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    evZmianaPietraUP();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedevZmianaPietraUP : virtual public AOMEvent {
    DECLARE_META_EVENT(evZmianaPietraUP)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.h
*********************************************************************/
