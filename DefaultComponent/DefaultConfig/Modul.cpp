/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Modul
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Modul.h"
//#[ ignore
#define Default_Modul_Modul_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Modul
Modul::Modul() {
    NOTIFY_CONSTRUCTOR(Modul, Modul(), 0, Default_Modul_Modul_SERIALIZE);
}

Modul::~Modul() {
    NOTIFY_DESTRUCTOR(~Modul, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(Modul, Default, Default, false, OMAnimatedModul)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Modul.cpp
*********************************************************************/
