/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Przycisk
//!	Generated Date	: Mon, 15, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Przycisk.h
*********************************************************************/

#ifndef Przycisk_H
#define Przycisk_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Przycisk
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Przycisk
class Przycisk : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedPrzycisk;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Przycisk(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Przycisk();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getWezwanie() const;
    
    //## auto_generated
    void setWezwanie(int p_wezwanie);
    
    //## auto_generated
    int getWybrane_pietro() const;
    
    //## auto_generated
    void setWybrane_pietro(int p_wybrane_pietro);
    
    ////    Attributes    ////

protected :

    int wezwanie;		//## attribute wezwanie
    
    int wybrane_pietro;		//## attribute wybrane_pietro
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_3:
    //## statechart_method
    inline bool sendaction_3_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Aktywny_handleEvent();
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Przycisk_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_3 = 2,
        Aktywny = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedPrzycisk : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Przycisk, OMAnimatedPrzycisk)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_3_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Przycisk::rootState_IN() const {
    return true;
}

inline bool Przycisk::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Przycisk::sendaction_3_IN() const {
    return rootState_subState == sendaction_3;
}

inline bool Przycisk::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Przycisk.h
*********************************************************************/
