/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Detektor_kolizji
//!	Generated Date	: Mon, 1, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Detektor_kolizji.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Detektor_kolizji.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Detektor_kolizji_Detektor_kolizji_SERIALIZE OM_NO_OP

#define Default_Detektor_kolizji_SprawdzKolizje_SERIALIZE OM_NO_OP

#define Default_Detektor_kolizji_zerujIloscImpulsow_SERIALIZE OM_NO_OP

#define Default_Detektor_kolizji_zerujLicznik_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Detektor_kolizji
Detektor_kolizji::Detektor_kolizji(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Detektor_kolizji, Detektor_kolizji(), 0, Default_Detektor_kolizji_Detektor_kolizji_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Detektor_kolizji::~Detektor_kolizji() {
    NOTIFY_DESTRUCTOR(~Detektor_kolizji, false);
    cleanUpRelations();
}

bool Detektor_kolizji::SprawdzKolizje() {
    NOTIFY_OPERATION(SprawdzKolizje, SprawdzKolizje(), 0, Default_Detektor_kolizji_SprawdzKolizje_SERIALIZE);
    //#[ operation SprawdzKolizje()
    liczba_impulsow_wyslanych++;  
    
    if(!czujnik_odb_nieaktywny)
    liczba_impulsow_odebranych++;      
    
    std::cout << "CZUJNIK NAD. " <<liczba_impulsow_wyslanych << "CZUJNIK ODB. " << liczba_impulsow_odebranych<<std::endl; 
    
    
    return ((liczba_impulsow_wyslanych - liczba_impulsow_odebranych)>prog);
    //#]
}

void Detektor_kolizji::zerujIloscImpulsow() {
    NOTIFY_OPERATION(zerujIloscImpulsow, zerujIloscImpulsow(), 0, Default_Detektor_kolizji_zerujIloscImpulsow_SERIALIZE);
    //#[ operation zerujIloscImpulsow()
    //#]
}

void Detektor_kolizji::zerujLicznik() {
    NOTIFY_OPERATION(zerujLicznik, zerujLicznik(), 0, Default_Detektor_kolizji_zerujLicznik_SERIALIZE);
    //#[ operation zerujLicznik()
    //#]
}

Kontroler_Windy* Detektor_kolizji::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Detektor_kolizji::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Detektor_kolizji::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Detektor_kolizji::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Detektor_kolizji::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

bool Detektor_kolizji::getCzujnik_odb_nieaktywny() const {
    return czujnik_odb_nieaktywny;
}

void Detektor_kolizji::setCzujnik_odb_nieaktywny(bool p_czujnik_odb_nieaktywny) {
    czujnik_odb_nieaktywny = p_czujnik_odb_nieaktywny;
}

Stany Detektor_kolizji::getKierunek() const {
    return kierunek;
}

void Detektor_kolizji::setKierunek(Stany p_kierunek) {
    kierunek = p_kierunek;
}

int Detektor_kolizji::getLiczba_impulsow_odebranych() const {
    return liczba_impulsow_odebranych;
}

void Detektor_kolizji::setLiczba_impulsow_odebranych(int p_liczba_impulsow_odebranych) {
    liczba_impulsow_odebranych = p_liczba_impulsow_odebranych;
}

int Detektor_kolizji::getLiczba_impulsow_wyslanych() const {
    return liczba_impulsow_wyslanych;
}

void Detektor_kolizji::setLiczba_impulsow_wyslanych(int p_liczba_impulsow_wyslanych) {
    liczba_impulsow_wyslanych = p_liczba_impulsow_wyslanych;
}

int Detektor_kolizji::getProg() const {
    return prog;
}

void Detektor_kolizji::setProg(int p_prog) {
    prog = p_prog;
}

void Detektor_kolizji::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Detektor_kolizji::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Detektor_kolizji::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Detektor_kolizji::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        //#[ state Wylaczony.(Entry) 
        czujnik_odb_nieaktywny = false;
        //#]
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Detektor_kolizji::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    //#[ transition 1 
                     kierunek = params -> stan;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            res = Aktywny_handleEvent();
        }
        break;
        // State sendaction_2
        case sendaction_2:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_2");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

IOxfReactive::TakeEventStatus Detektor_kolizji::Aktywny_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evKolizja_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
            pushNullTransition();
            rootState_subState = sendaction_2;
            rootState_active = sendaction_2;
            //#[ state sendaction_2.(Entry) 
            itsKontroler_Windy->GEN(evKolizja);
            //#]
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evStop_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 2 
            zerujLicznik();
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
            rootState_subState = Wylaczony;
            rootState_active = Wylaczony;
            //#[ state Wylaczony.(Entry) 
            czujnik_odb_nieaktywny = false;
            //#]
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev_zabl_czuj_odb_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evSprawdzKolizje_Default_id))
        {
            //## transition 6 
            if(SprawdzKolizje())
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_2");
                    pushNullTransition();
                    rootState_subState = sendaction_2;
                    rootState_active = sendaction_2;
                    //#[ state sendaction_2.(Entry) 
                    itsKontroler_Windy->GEN(evKolizja);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
        }
    
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedDetektor_kolizji::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("czujnik_odb_nieaktywny", x2String(myReal->czujnik_odb_nieaktywny));
    aomsAttributes->addAttribute("kierunek", x2String((int)myReal->kierunek));
    aomsAttributes->addAttribute("liczba_impulsow_odebranych", x2String(myReal->liczba_impulsow_odebranych));
    aomsAttributes->addAttribute("liczba_impulsow_wyslanych", x2String(myReal->liczba_impulsow_wyslanych));
    aomsAttributes->addAttribute("prog", x2String(myReal->prog));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedDetektor_kolizji::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedDetektor_kolizji::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Detektor_kolizji::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Detektor_kolizji::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Detektor_kolizji::sendaction_2:
        {
            sendaction_2_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedDetektor_kolizji::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedDetektor_kolizji::sendaction_2_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_2");
}

void OMAnimatedDetektor_kolizji::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Detektor_kolizji, Default, false, Modul, OMAnimatedModul, OMAnimatedDetektor_kolizji)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Detektor_kolizji.cpp
*********************************************************************/
