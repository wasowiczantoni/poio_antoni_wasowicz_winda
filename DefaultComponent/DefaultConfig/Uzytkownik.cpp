/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Uzytkownik
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Uzytkownik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Uzytkownik.h"
//#[ ignore
#define Default_Uzytkownik_Uzytkownik_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Uzytkownik
Uzytkownik::Uzytkownik() {
    NOTIFY_CONSTRUCTOR(Uzytkownik, Uzytkownik(), 0, Default_Uzytkownik_Uzytkownik_SERIALIZE);
}

Uzytkownik::~Uzytkownik() {
    NOTIFY_DESTRUCTOR(~Uzytkownik, true);
}

#ifdef _OMINSTRUMENT
IMPLEMENT_META_P(Uzytkownik, Default, Default, false, OMAnimatedUzytkownik)
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Uzytkownik.cpp
*********************************************************************/
