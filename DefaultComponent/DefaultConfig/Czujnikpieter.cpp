/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Czujnikpieter
//!	Generated Date	: Fri, 2, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Czujnikpieter.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Czujnikpieter.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Czujnikpieter_Czujnikpieter_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Czujnikpieter
Czujnikpieter::Czujnikpieter(IOxfActive* theActiveContext) : aktualne_pietro(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Czujnikpieter, Czujnikpieter(), 0, Default_Czujnikpieter_Czujnikpieter_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Czujnikpieter::~Czujnikpieter() {
    NOTIFY_DESTRUCTOR(~Czujnikpieter, false);
    cleanUpRelations();
}

Kontroler_Windy* Czujnikpieter::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Czujnikpieter::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    if(p_Kontroler_Windy != NULL)
        {
            p_Kontroler_Windy->_setItsCzujnikpieter_1(this);
        }
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Czujnikpieter::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

Stany Czujnikpieter::getStan_windy_aktualny() const {
    return stan_windy_aktualny;
}

void Czujnikpieter::setStan_windy_aktualny(Stany p_stan_windy_aktualny) {
    stan_windy_aktualny = p_stan_windy_aktualny;
}

void Czujnikpieter::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Czujnikpieter::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            Czujnikpieter* p_Czujnikpieter = itsKontroler_Windy->getItsCzujnikpieter_1();
            if(p_Czujnikpieter != NULL)
                {
                    itsKontroler_Windy->__setItsCzujnikpieter_1(NULL);
                }
            itsKontroler_Windy = NULL;
        }
}

unsigned int Czujnikpieter::getAktualne_pietro() const {
    return aktualne_pietro;
}

void Czujnikpieter::setAktualne_pietro(unsigned int p_aktualne_pietro) {
    aktualne_pietro = p_aktualne_pietro;
}

int Czujnikpieter::getKrok_drzwi() const {
    return krok_drzwi;
}

void Czujnikpieter::setKrok_drzwi(int p_krok_drzwi) {
    krok_drzwi = p_krok_drzwi;
}

void Czujnikpieter::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Czujnikpieter::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    if(itsKontroler_Windy != NULL)
        {
            itsKontroler_Windy->__setItsCzujnikpieter_1(NULL);
        }
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Czujnikpieter::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Czujnikpieter::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        pushNullTransition();
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Czujnikpieter::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(evZmianaPietraDOWN_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_w_dol");
                    pushNullTransition();
                    rootState_subState = Jazda_w_dol;
                    rootState_active = Jazda_w_dol;
                    //#[ state Jazda_w_dol.(Entry) 
                    aktualne_pietro--;
                    std::cout<<aktualne_pietro<<std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evZmianaPietraUP_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_w_gore");
                    pushNullTransition();
                    rootState_subState = Jazda_w_gore;
                    rootState_active = Jazda_w_gore;
                    //#[ state Jazda_w_gore.(Entry) 
                    aktualne_pietro++;                    
                    std::cout<<"Aktualne pietro: "<<aktualne_pietro<<std::endl<<std::endl;
                    
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda_w_gore
        case Jazda_w_gore:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("4");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_w_gore");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_4");
                    pushNullTransition();
                    rootState_subState = sendaction_4;
                    rootState_active = sendaction_4;
                    //#[ state sendaction_4.(Entry) 
                    itsKontroler_Windy->GEN(evStart());
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("4");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda_w_dol
        case Jazda_w_dol:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_w_dol");
                    NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
                    pushNullTransition();
                    rootState_subState = sendaction_5;
                    rootState_active = sendaction_5;
                    //#[ state sendaction_5.(Entry) 
                    itsKontroler_Windy->GEN(evKrok());
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_4
        case sendaction_4:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_4");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedCzujnikpieter::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("stan_windy_aktualny", x2String((int)myReal->stan_windy_aktualny));
    aomsAttributes->addAttribute("krok_drzwi", x2String(myReal->krok_drzwi));
    aomsAttributes->addAttribute("aktualne_pietro", x2String(myReal->aktualne_pietro));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedCzujnikpieter::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedCzujnikpieter::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Czujnikpieter::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Czujnikpieter::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Czujnikpieter::Jazda_w_gore:
        {
            Jazda_w_gore_serializeStates(aomsState);
        }
        break;
        case Czujnikpieter::Jazda_w_dol:
        {
            Jazda_w_dol_serializeStates(aomsState);
        }
        break;
        case Czujnikpieter::sendaction_4:
        {
            sendaction_4_serializeStates(aomsState);
        }
        break;
        case Czujnikpieter::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedCzujnikpieter::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedCzujnikpieter::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedCzujnikpieter::sendaction_4_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_4");
}

void OMAnimatedCzujnikpieter::Jazda_w_gore_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_w_gore");
}

void OMAnimatedCzujnikpieter::Jazda_w_dol_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_w_dol");
}

void OMAnimatedCzujnikpieter::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Czujnikpieter, Default, false, Modul, OMAnimatedModul, OMAnimatedCzujnikpieter)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Czujnikpieter.cpp
*********************************************************************/
