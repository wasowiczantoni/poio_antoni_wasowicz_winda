/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Detektor_kolizji
//!	Generated Date	: Mon, 1, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Detektor_kolizji.h
*********************************************************************/

#ifndef Detektor_kolizji_H
#define Detektor_kolizji_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Detektor_kolizji
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Detektor_kolizji
class Detektor_kolizji : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedDetektor_kolizji;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Detektor_kolizji(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Detektor_kolizji();
    
    ////    Operations    ////
    
    //## operation SprawdzKolizje()
    bool SprawdzKolizje();
    
    //## operation zerujIloscImpulsow()
    void zerujIloscImpulsow();
    
    //## operation zerujLicznik()
    void zerujLicznik();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    bool getCzujnik_odb_nieaktywny() const;
    
    //## auto_generated
    void setCzujnik_odb_nieaktywny(bool p_czujnik_odb_nieaktywny);
    
    //## auto_generated
    Stany getKierunek() const;
    
    //## auto_generated
    void setKierunek(Stany p_kierunek);
    
    //## auto_generated
    int getLiczba_impulsow_odebranych() const;
    
    //## auto_generated
    void setLiczba_impulsow_odebranych(int p_liczba_impulsow_odebranych);
    
    //## auto_generated
    int getLiczba_impulsow_wyslanych() const;
    
    //## auto_generated
    void setLiczba_impulsow_wyslanych(int p_liczba_impulsow_wyslanych);
    
    //## auto_generated
    int getProg() const;
    
    //## auto_generated
    void setProg(int p_prog);
    
    ////    Attributes    ////

protected :

    bool czujnik_odb_nieaktywny;		//## attribute czujnik_odb_nieaktywny
    
    Stany kierunek;		//## attribute kierunek
    
    int liczba_impulsow_odebranych;		//## attribute liczba_impulsow_odebranych
    
    int liczba_impulsow_wyslanych;		//## attribute liczba_impulsow_wyslanych
    
    int prog;		//## attribute prog
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_2:
    //## statechart_method
    inline bool sendaction_2_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Aktywny_handleEvent();
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Detektor_kolizji_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_2 = 2,
        Aktywny = 3
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedDetektor_kolizji : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Detektor_kolizji, OMAnimatedDetektor_kolizji)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_2_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Detektor_kolizji::rootState_IN() const {
    return true;
}

inline bool Detektor_kolizji::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Detektor_kolizji::sendaction_2_IN() const {
    return rootState_subState == sendaction_2;
}

inline bool Detektor_kolizji::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Detektor_kolizji.h
*********************************************************************/
