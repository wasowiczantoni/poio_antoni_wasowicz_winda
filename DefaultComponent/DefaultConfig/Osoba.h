/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Osoba
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Osoba.h
*********************************************************************/

#ifndef Osoba_H
#define Osoba_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## actor Osoba
#include "Uzytkownik.h"
//## package Default

//## actor Osoba
class Osoba : public Uzytkownik {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedOsoba;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Osoba();
    
    //## auto_generated
    ~Osoba();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedOsoba : public OMAnimatedUzytkownik {
    DECLARE_META(Osoba, OMAnimatedOsoba)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Osoba.h
*********************************************************************/
