/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: actor_1
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/actor_1.h
*********************************************************************/

#ifndef actor_1_H
#define actor_1_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## actor actor_1
class actor_1 {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedactor_1;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    actor_1();
    
    //## auto_generated
    ~actor_1();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedactor_1 : virtual public AOMInstance {
    DECLARE_META(actor_1, OMAnimatedactor_1)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/actor_1.h
*********************************************************************/
