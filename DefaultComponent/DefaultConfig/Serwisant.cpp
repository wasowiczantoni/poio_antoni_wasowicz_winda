/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Serwisant
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Serwisant.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Serwisant.h"
//#[ ignore
#define Default_Serwisant_Serwisant_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Serwisant
Serwisant::Serwisant() {
    NOTIFY_CONSTRUCTOR(Serwisant, Serwisant(), 0, Default_Serwisant_Serwisant_SERIALIZE);
}

Serwisant::~Serwisant() {
    NOTIFY_DESTRUCTOR(~Serwisant, false);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSerwisant::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedUzytkownik::serializeAttributes(aomsAttributes);
}

void OMAnimatedSerwisant::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedUzytkownik::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(Serwisant, Default, false, Uzytkownik, OMAnimatedUzytkownik, OMAnimatedSerwisant)

OMINIT_SUPERCLASS(Uzytkownik, OMAnimatedUzytkownik)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Serwisant.cpp
*********************************************************************/
