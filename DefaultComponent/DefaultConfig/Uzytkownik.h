/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Uzytkownik
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Uzytkownik.h
*********************************************************************/

#ifndef Uzytkownik_H
#define Uzytkownik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## package Default

//## actor Uzytkownik
class Uzytkownik {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedUzytkownik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Uzytkownik();
    
    //## auto_generated
    ~Uzytkownik();
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedUzytkownik : virtual public AOMInstance {
    DECLARE_META(Uzytkownik, OMAnimatedUzytkownik)
};
//#]
#endif // _OMINSTRUMENT

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Uzytkownik.h
*********************************************************************/
