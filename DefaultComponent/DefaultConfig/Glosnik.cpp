/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Glosnik
//!	Generated Date	: Mon, 15, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Glosnik.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Glosnik.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Glosnik_Glosnik_SERIALIZE OM_NO_OP

#define Default_Glosnik_Info_o_pietrze_SERIALIZE aomsmethod->addAttribute("pietro", x2String(pietro));
//#]

//## package Default

//## class Glosnik
Glosnik::Glosnik(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Glosnik, Glosnik(), 0, Default_Glosnik_Glosnik_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Glosnik::~Glosnik() {
    NOTIFY_DESTRUCTOR(~Glosnik, false);
    cleanUpRelations();
}

void Glosnik::Info_o_pietrze(int pietro) {
    NOTIFY_OPERATION(Info_o_pietrze, Info_o_pietrze(int), 1, Default_Glosnik_Info_o_pietrze_SERIALIZE);
    //#[ operation Info_o_pietrze(int)
    //#]
}

Kontroler_Windy* Glosnik::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Glosnik::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Glosnik::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Glosnik::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Glosnik::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

void Glosnik::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Glosnik::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Glosnik::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Glosnik::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Glosnik::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    pushNullTransition();
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    if(TRUE)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            NOTIFY_TRANSITION_STARTED("11");
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Aktywny");
                            NOTIFY_STATE_ENTERED("ROOT.Zamykanie");
                            pushNullTransition();
                            rootState_subState = Zamykanie;
                            rootState_active = Zamykanie;
                            //#[ state Zamykanie.(Entry) 
                            std::cout<<"Drzwi zamykaja sie"<<std::endl;
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("11");
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Otwieranie
        case Otwieranie:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Otwieranie");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    pushNullTransition();
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Zamykanie
        case Zamykanie:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("9");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zamykanie");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    pushNullTransition();
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("9");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda_w_dol
        case Jazda_w_dol:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_w_dol");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    pushNullTransition();
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Jazda_w_gore
        case Jazda_w_gore:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_w_gore");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    pushNullTransition();
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedGlosnik::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedGlosnik::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedGlosnik::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Glosnik::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Glosnik::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Glosnik::Otwieranie:
        {
            Otwieranie_serializeStates(aomsState);
        }
        break;
        case Glosnik::Zamykanie:
        {
            Zamykanie_serializeStates(aomsState);
        }
        break;
        case Glosnik::Jazda_w_dol:
        {
            Jazda_w_dol_serializeStates(aomsState);
        }
        break;
        case Glosnik::Jazda_w_gore:
        {
            Jazda_w_gore_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedGlosnik::Zamykanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zamykanie");
}

void OMAnimatedGlosnik::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedGlosnik::Otwieranie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Otwieranie");
}

void OMAnimatedGlosnik::Jazda_w_gore_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_w_gore");
}

void OMAnimatedGlosnik::Jazda_w_dol_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_w_dol");
}

void OMAnimatedGlosnik::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Glosnik, Default, false, Modul, OMAnimatedModul, OMAnimatedGlosnik)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Glosnik.cpp
*********************************************************************/
