/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: SilnikWindy
//!	Generated Date	: Fri, 2, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/SilnikWindy.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "SilnikWindy.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_SilnikWindy_SilnikWindy_SERIALIZE OM_NO_OP

#define Default_SilnikWindy_Jazda_w_gore_SERIALIZE OM_NO_OP

#define Default_SilnikWindy_jazda_w_dol_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class SilnikWindy
SilnikWindy::SilnikWindy(IOxfActive* theActiveContext) : kierunek(GORA), okres(600), predkosc(1.5) {
    NOTIFY_REACTIVE_CONSTRUCTOR(SilnikWindy, SilnikWindy(), 0, Default_SilnikWindy_SilnikWindy_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

SilnikWindy::~SilnikWindy() {
    NOTIFY_DESTRUCTOR(~SilnikWindy, false);
    cleanUpRelations();
    cancelTimeouts();
}

void SilnikWindy::Jazda_w_gore() {
    NOTIFY_OPERATION(Jazda_w_gore, Jazda_w_gore(), 0, Default_SilnikWindy_Jazda_w_gore_SERIALIZE);
    //#[ operation Jazda_w_gore()
    //#]
}

void SilnikWindy::jazda_w_dol() {
    NOTIFY_OPERATION(jazda_w_dol, jazda_w_dol(), 0, Default_SilnikWindy_jazda_w_dol_SERIALIZE);
    //#[ operation jazda_w_dol()
    //#]
}

int SilnikWindy::getOkres() const {
    return okres;
}

void SilnikWindy::setOkres(int p_okres) {
    okres = p_okres;
}

double SilnikWindy::getPredkosc() const {
    return predkosc;
}

void SilnikWindy::setPredkosc(double p_predkosc) {
    predkosc = p_predkosc;
}

Kontroler_Windy* SilnikWindy::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void SilnikWindy::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool SilnikWindy::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void SilnikWindy::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void SilnikWindy::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

void SilnikWindy::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool SilnikWindy::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

Stany SilnikWindy::getKierunek() const {
    return kierunek;
}

void SilnikWindy::setKierunek(Stany p_kierunek) {
    kierunek = p_kierunek;
}

void SilnikWindy::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void SilnikWindy::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void SilnikWindy::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void SilnikWindy::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        pushNullTransition();
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus SilnikWindy::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        // State DOWN
        case DOWN:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.DOWN");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
                            pushNullTransition();
                            rootState_subState = sendaction_5;
                            rootState_active = sendaction_5;
                            //#[ state sendaction_5.(Entry) 
                            itsKontroler_Windy->GEN(evZmianaPietraDOWN);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State UP
        case UP:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("4");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.UP");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsKontroler_Windy->GEN(evZmianaPietraUP);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("4");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(jedz_w_gore_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.UP");
                    rootState_subState = UP;
                    rootState_active = UP;
                    //#[ state UP.(Entry) 
                    kierunek = GORA; 
                    std::cout<<"Jazda w gore"<<std::endl<<std::endl;
                    //#]
                    rootState_timeout = scheduleTimeout(4000, "ROOT.UP");
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(jedz_w_dol_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.DOWN");
                    rootState_subState = DOWN;
                    rootState_active = DOWN;
                    //#[ state DOWN.(Entry) 
                    kierunek = DOL;
                    std::cout<<"Jazda w dol"<<std::endl<<std::endl;
                    //#]
                    rootState_timeout = scheduleTimeout(4000, "ROOT.DOWN");
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("6");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("6");
                    res = eventConsumed;
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSilnikWindy::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("kierunek", x2String((int)myReal->kierunek));
    aomsAttributes->addAttribute("predkosc", x2String(myReal->predkosc));
    aomsAttributes->addAttribute("okres", x2String(myReal->okres));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedSilnikWindy::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedSilnikWindy::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case SilnikWindy::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case SilnikWindy::DOWN:
        {
            DOWN_serializeStates(aomsState);
        }
        break;
        case SilnikWindy::UP:
        {
            UP_serializeStates(aomsState);
        }
        break;
        case SilnikWindy::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case SilnikWindy::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        case SilnikWindy::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSilnikWindy::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedSilnikWindy::UP_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.UP");
}

void OMAnimatedSilnikWindy::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_6");
}

void OMAnimatedSilnikWindy::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedSilnikWindy::DOWN_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.DOWN");
}

void OMAnimatedSilnikWindy::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(SilnikWindy, Default, false, Modul, OMAnimatedModul, OMAnimatedSilnikWindy)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/SilnikWindy.cpp
*********************************************************************/
