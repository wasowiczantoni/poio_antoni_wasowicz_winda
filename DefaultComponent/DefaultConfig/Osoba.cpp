/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Osoba
//!	Generated Date	: Wed, 15, Jun 2022  
	File Path	: DefaultComponent/DefaultConfig/Osoba.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Osoba.h"
//#[ ignore
#define Default_Osoba_Osoba_SERIALIZE OM_NO_OP
//#]

//## package Default

//## actor Osoba
Osoba::Osoba() {
    NOTIFY_CONSTRUCTOR(Osoba, Osoba(), 0, Default_Osoba_Osoba_SERIALIZE);
}

Osoba::~Osoba() {
    NOTIFY_DESTRUCTOR(~Osoba, false);
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedOsoba::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedUzytkownik::serializeAttributes(aomsAttributes);
}

void OMAnimatedOsoba::serializeRelations(AOMSRelations* aomsRelations) const {
    OMAnimatedUzytkownik::serializeRelations(aomsRelations);
}
//#]

IMPLEMENT_META_S_P(Osoba, Default, false, Uzytkownik, OMAnimatedUzytkownik, OMAnimatedOsoba)

OMINIT_SUPERCLASS(Uzytkownik, OMAnimatedUzytkownik)

OMREGISTER_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Osoba.cpp
*********************************************************************/
