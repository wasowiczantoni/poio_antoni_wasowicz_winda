/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: SilnikWindy
//!	Generated Date	: Mon, 1, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/SilnikWindy.h
*********************************************************************/

#ifndef SilnikWindy_H
#define SilnikWindy_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class SilnikWindy
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class SilnikWindy
class SilnikWindy : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedSilnikWindy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    SilnikWindy(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~SilnikWindy();
    
    ////    Operations    ////
    
    //## operation Jazda_w_gore()
    void Jazda_w_gore();
    
    //## operation jazda_w_dol()
    void jazda_w_dol();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getOkres() const;
    
    //## auto_generated
    void setOkres(int p_okres);
    
    //## auto_generated
    double getPredkosc() const;
    
    //## auto_generated
    void setPredkosc(double p_predkosc);
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cancelTimeouts();
    
    //## auto_generated
    bool cancelTimeout(const IOxfTimeout* arg);

private :

    //## auto_generated
    Stany getKierunek() const;
    
    //## auto_generated
    void setKierunek(Stany p_kierunek);
    
    ////    Attributes    ////

protected :

    Stany kierunek;		//## attribute kierunek
    
    int okres;		//## attribute okres
    
    double predkosc;		//## attribute predkosc
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // UP:
    //## statechart_method
    inline bool UP_IN() const;
    
    // sendaction_6:
    //## statechart_method
    inline bool sendaction_6_IN() const;
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    // DOWN:
    //## statechart_method
    inline bool DOWN_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum SilnikWindy_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        UP = 2,
        sendaction_6 = 3,
        sendaction_5 = 4,
        DOWN = 5,
        Aktywny = 6
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    IOxfTimeout* rootState_timeout;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedSilnikWindy : public OMAnimatedModul {
    DECLARE_REACTIVE_META(SilnikWindy, OMAnimatedSilnikWindy)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void UP_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_6_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void DOWN_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool SilnikWindy::rootState_IN() const {
    return true;
}

inline bool SilnikWindy::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool SilnikWindy::UP_IN() const {
    return rootState_subState == UP;
}

inline bool SilnikWindy::sendaction_6_IN() const {
    return rootState_subState == sendaction_6;
}

inline bool SilnikWindy::sendaction_5_IN() const {
    return rootState_subState == sendaction_5;
}

inline bool SilnikWindy::DOWN_IN() const {
    return rootState_subState == DOWN;
}

inline bool SilnikWindy::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/SilnikWindy.h
*********************************************************************/
