/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Kontroler_Windy
//!	Generated Date	: Fri, 2, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Kontroler_Windy.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Kontroler_Windy_Kontroler_Windy_SERIALIZE OM_NO_OP

#define Default_Kontroler_Windy_Dekodowanie_SERIALIZE OM_NO_OP

#define Default_Kontroler_Windy_Drzwi_zamkniete_SERIALIZE OM_NO_OP

#define Default_Kontroler_Windy_PietroWezwania_SERIALIZE OM_NO_OP

#define Default_Kontroler_Windy_ustawDrzwi_SERIALIZE OM_NO_OP

#define Default_Kontroler_Windy_ustawStan_SERIALIZE OM_NO_OP

#define Default_itsCzujnikpieter_Czujnikpieter_itsCzujnikpieter_Czujnikpieter_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Kontroler_Windy
//## class Kontroler_Windy::itsCzujnikpieter_Czujnikpieter
Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::itsCzujnikpieter_Czujnikpieter_C() {
    NOTIFY_CONSTRUCTOR_OBJECT(itsCzujnikpieter_Czujnikpieter, itsCzujnikpieter_Czujnikpieter(), 0, Default_itsCzujnikpieter_Czujnikpieter_itsCzujnikpieter_Czujnikpieter_SERIALIZE, itsCzujnikpieter_Czujnikpieter_C);
    itsKontroler_Windy = NULL;
}

Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::~itsCzujnikpieter_Czujnikpieter_C() {
    NOTIFY_DESTRUCTOR(~itsCzujnikpieter_Czujnikpieter, true);
    cleanUpRelations();
}

Kontroler_Windy* Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    if(p_Kontroler_Windy != NULL)
        {
            p_Kontroler_Windy->_setItsItsCzujnikpieter_Czujnikpieter(this);
        }
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

void Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter = itsKontroler_Windy->getItsItsCzujnikpieter_Czujnikpieter();
            if(p_itsCzujnikpieter_Czujnikpieter != NULL)
                {
                    itsKontroler_Windy->__setItsItsCzujnikpieter_Czujnikpieter(NULL);
                }
            itsKontroler_Windy = NULL;
        }
}

void Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    if(itsKontroler_Windy != NULL)
        {
            itsKontroler_Windy->__setItsItsCzujnikpieter_Czujnikpieter(NULL);
        }
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

Kontroler_Windy::Kontroler_Windy(IOxfActive* theActiveContext) : aktualne_pietro(0), pozycja(0), pozycja_drzwi(1.0), pozycja_max_drzwi(2.0), pozycja_max_winda(4), pozycja_min_drzwi(0.0), pozycja_min_winda(-1), stan_aktualny_drzwi(ZAMKNIETE), stan_poprzedni_drzwi(ZAMKNIETE), stan_windy(ZATRZYMANIE), i(0), stan_windy_aktualny(ZATRZYMANIE) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Kontroler_Windy, Kontroler_Windy(), 0, Default_Kontroler_Windy_Kontroler_Windy_SERIALIZE);
    setActiveContext(theActiveContext, false);
    {
        {
            itsDetektor_kolizji.setShouldDelete(false);
        }
        {
            itsPanel.setShouldDelete(false);
        }
        {
            itsPrzycisk.setShouldDelete(false);
        }
        {
            itsSilnikDrzwi.setShouldDelete(false);
        }
        {
            itsWyswietlacz.setShouldDelete(false);
        }
        {
            itsSilnikWindy.setShouldDelete(false);
        }
        {
            itsSilnik_drzwi.setShouldDelete(false);
        }
        {
            itsGlosnik.setShouldDelete(false);
        }
        {
            itsCzujnikpieter.setShouldDelete(false);
        }
    }
    itsCzujnikpieter_1 = NULL;
    itsDetektor_kolizji_1 = NULL;
    itsGlosnik_1 = NULL;
    itsItsCzujnikpieter_Czujnikpieter = NULL;
    itsPanel_1 = NULL;
    itsPrzycisk_1 = NULL;
    itsSilnikWindy_1 = NULL;
    itsSilnik_drzwi_1 = NULL;
    itsWyswietlacz_1 = NULL;
    initRelations();
    initStatechart();
}

Kontroler_Windy::~Kontroler_Windy() {
    NOTIFY_DESTRUCTOR(~Kontroler_Windy, true);
    cleanUpRelations();
}

void Kontroler_Windy::Dekodowanie() {
    NOTIFY_OPERATION(Dekodowanie, Dekodowanie(), 0, Default_Kontroler_Windy_Dekodowanie_SERIALIZE);
    //#[ operation Dekodowanie()
    //#]
}

void Kontroler_Windy::PietroWezwania() {
    NOTIFY_OPERATION(PietroWezwania, PietroWezwania(), 0, Default_Kontroler_Windy_PietroWezwania_SERIALIZE);
    //#[ operation PietroWezwania()
    //#]
}

void Kontroler_Windy::ustawDrzwi() {
    NOTIFY_OPERATION(ustawDrzwi, ustawDrzwi(), 0, Default_Kontroler_Windy_ustawDrzwi_SERIALIZE);
    //#[ operation ustawDrzwi()
    //#]
}

void Kontroler_Windy::ustawStan() {
    NOTIFY_OPERATION(ustawStan, ustawStan(), 0, Default_Kontroler_Windy_ustawStan_SERIALIZE);
    //#[ operation ustawStan()
    //#]
}

int Kontroler_Windy::getAktualne_pietro() const {
    return aktualne_pietro;
}

void Kontroler_Windy::setAktualne_pietro(int p_aktualne_pietro) {
    aktualne_pietro = p_aktualne_pietro;
    NOTIFY_SET_OPERATION;
}

Stany Kontroler_Windy::getStan_aktualny_drzwi() const {
    return stan_aktualny_drzwi;
}

void Kontroler_Windy::setStan_aktualny_drzwi(Stany p_stan_aktualny_drzwi) {
    stan_aktualny_drzwi = p_stan_aktualny_drzwi;
}

Stany Kontroler_Windy::getStan_poprzedni_drzwi() const {
    return stan_poprzedni_drzwi;
}

void Kontroler_Windy::setStan_poprzedni_drzwi(Stany p_stan_poprzedni_drzwi) {
    stan_poprzedni_drzwi = p_stan_poprzedni_drzwi;
}

Stany Kontroler_Windy::getStan_windy_aktualny() const {
    return stan_windy_aktualny;
}

void Kontroler_Windy::setStan_windy_aktualny(Stany p_stan_windy_aktualny) {
    stan_windy_aktualny = p_stan_windy_aktualny;
}

int Kontroler_Windy::getWezwanie() const {
    return wezwanie;
}

void Kontroler_Windy::setWezwanie(int p_wezwanie) {
    wezwanie = p_wezwanie;
}

int Kontroler_Windy::getWybrane_pietro() const {
    return wybrane_pietro;
}

void Kontroler_Windy::setWybrane_pietro(int p_wybrane_pietro) {
    wybrane_pietro = p_wybrane_pietro;
}

Czujnikpieter* Kontroler_Windy::getItsCzujnikpieter() const {
    return (Czujnikpieter*) &itsCzujnikpieter;
}

Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* Kontroler_Windy::getItsCzujnikpieter_Czujnikpieter() const {
    return (Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C*) &itsCzujnikpieter_Czujnikpieter;
}

Detektor_kolizji* Kontroler_Windy::getItsDetektor_kolizji() const {
    return (Detektor_kolizji*) &itsDetektor_kolizji;
}

Detektor_kolizji* Kontroler_Windy::getItsDetektor_kolizji_1() const {
    return itsDetektor_kolizji_1;
}

void Kontroler_Windy::setItsDetektor_kolizji_1(Detektor_kolizji* p_Detektor_kolizji) {
    itsDetektor_kolizji_1 = p_Detektor_kolizji;
    if(p_Detektor_kolizji != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsDetektor_kolizji_1", p_Detektor_kolizji, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsDetektor_kolizji_1");
        }
}

Glosnik* Kontroler_Windy::getItsGlosnik() const {
    return (Glosnik*) &itsGlosnik;
}

Glosnik* Kontroler_Windy::getItsGlosnik_1() const {
    return itsGlosnik_1;
}

void Kontroler_Windy::setItsGlosnik_1(Glosnik* p_Glosnik) {
    itsGlosnik_1 = p_Glosnik;
    if(p_Glosnik != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsGlosnik_1", p_Glosnik, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsGlosnik_1");
        }
}

Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* Kontroler_Windy::getItsItsCzujnikpieter_Czujnikpieter() const {
    return itsItsCzujnikpieter_Czujnikpieter;
}

Panel* Kontroler_Windy::getItsPanel() const {
    return (Panel*) &itsPanel;
}

Panel* Kontroler_Windy::getItsPanel_1() const {
    return itsPanel_1;
}

void Kontroler_Windy::setItsPanel_1(Panel* p_Panel) {
    itsPanel_1 = p_Panel;
    if(p_Panel != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsPanel_1", p_Panel, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsPanel_1");
        }
}

Przycisk* Kontroler_Windy::getItsPrzycisk() const {
    return (Przycisk*) &itsPrzycisk;
}

Przycisk* Kontroler_Windy::getItsPrzycisk_1() const {
    return itsPrzycisk_1;
}

void Kontroler_Windy::setItsPrzycisk_1(Przycisk* p_Przycisk) {
    itsPrzycisk_1 = p_Przycisk;
    if(p_Przycisk != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsPrzycisk_1", p_Przycisk, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsPrzycisk_1");
        }
}

Silnik_drzwi* Kontroler_Windy::getItsSilnikDrzwi() const {
    return (Silnik_drzwi*) &itsSilnikDrzwi;
}

SilnikWindy* Kontroler_Windy::getItsSilnikWindy() const {
    return (SilnikWindy*) &itsSilnikWindy;
}

SilnikWindy* Kontroler_Windy::getItsSilnikWindy_1() const {
    return itsSilnikWindy_1;
}

void Kontroler_Windy::setItsSilnikWindy_1(SilnikWindy* p_SilnikWindy) {
    itsSilnikWindy_1 = p_SilnikWindy;
    if(p_SilnikWindy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSilnikWindy_1", p_SilnikWindy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSilnikWindy_1");
        }
}

Silnik_drzwi* Kontroler_Windy::getItsSilnik_drzwi() const {
    return (Silnik_drzwi*) &itsSilnik_drzwi;
}

Silnik_drzwi* Kontroler_Windy::getItsSilnik_drzwi_1() const {
    return itsSilnik_drzwi_1;
}

void Kontroler_Windy::setItsSilnik_drzwi_1(Silnik_drzwi* p_Silnik_drzwi) {
    itsSilnik_drzwi_1 = p_Silnik_drzwi;
    if(p_Silnik_drzwi != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsSilnik_drzwi_1", p_Silnik_drzwi, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsSilnik_drzwi_1");
        }
}

Wyswietlacz* Kontroler_Windy::getItsWyswietlacz() const {
    return (Wyswietlacz*) &itsWyswietlacz;
}

Wyswietlacz* Kontroler_Windy::getItsWyswietlacz_1() const {
    return itsWyswietlacz_1;
}

void Kontroler_Windy::setItsWyswietlacz_1(Wyswietlacz* p_Wyswietlacz) {
    itsWyswietlacz_1 = p_Wyswietlacz;
    if(p_Wyswietlacz != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsWyswietlacz_1", p_Wyswietlacz, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsWyswietlacz_1");
        }
}

bool Kontroler_Windy::startBehavior() {
    bool done = true;
    done &= itsCzujnikpieter.startBehavior();
    done &= itsDetektor_kolizji.startBehavior();
    done &= itsGlosnik.startBehavior();
    done &= itsPanel.startBehavior();
    done &= itsPrzycisk.startBehavior();
    done &= itsSilnikDrzwi.startBehavior();
    done &= itsSilnikWindy.startBehavior();
    done &= itsSilnik_drzwi.startBehavior();
    done &= itsWyswietlacz.startBehavior();
    done &= OMReactive::startBehavior();
    return done;
}

void Kontroler_Windy::initRelations() {
    itsDetektor_kolizji._setItsKontroler_Windy(this);
    itsGlosnik._setItsKontroler_Windy(this);
    itsPanel._setItsKontroler_Windy(this);
    itsPrzycisk._setItsKontroler_Windy(this);
    itsSilnikWindy._setItsKontroler_Windy(this);
    itsSilnik_drzwi._setItsKontroler_Windy(this);
    itsWyswietlacz._setItsKontroler_Windy(this);
}

void Kontroler_Windy::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    Zamkniecie_drzwi_subState = OMNonState;
    Jazda_na_wybrane_pietro_subState = OMNonState;
    Jazda_na_wezwanie_subState = OMNonState;
}

void Kontroler_Windy::cleanUpRelations() {
    if(itsCzujnikpieter_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsCzujnikpieter_1");
            Kontroler_Windy* p_Kontroler_Windy = itsCzujnikpieter_1->getItsKontroler_Windy();
            if(p_Kontroler_Windy != NULL)
                {
                    itsCzujnikpieter_1->__setItsKontroler_Windy(NULL);
                }
            itsCzujnikpieter_1 = NULL;
        }
    if(itsDetektor_kolizji_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsDetektor_kolizji_1");
            itsDetektor_kolizji_1 = NULL;
        }
    if(itsGlosnik_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsGlosnik_1");
            itsGlosnik_1 = NULL;
        }
    if(itsItsCzujnikpieter_Czujnikpieter != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsItsCzujnikpieter_Czujnikpieter");
            Kontroler_Windy* p_Kontroler_Windy = itsItsCzujnikpieter_Czujnikpieter->getItsKontroler_Windy();
            if(p_Kontroler_Windy != NULL)
                {
                    itsItsCzujnikpieter_Czujnikpieter->__setItsKontroler_Windy(NULL);
                }
            itsItsCzujnikpieter_Czujnikpieter = NULL;
        }
    if(itsPanel_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsPanel_1");
            itsPanel_1 = NULL;
        }
    if(itsPrzycisk_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsPrzycisk_1");
            itsPrzycisk_1 = NULL;
        }
    if(itsSilnikWindy_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSilnikWindy_1");
            itsSilnikWindy_1 = NULL;
        }
    if(itsSilnik_drzwi_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsSilnik_drzwi_1");
            itsSilnik_drzwi_1 = NULL;
        }
    if(itsWyswietlacz_1 != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsWyswietlacz_1");
            itsWyswietlacz_1 = NULL;
        }
}

int Kontroler_Windy::getPozycja() const {
    return pozycja;
}

void Kontroler_Windy::setPozycja(int p_pozycja) {
    pozycja = p_pozycja;
}

double Kontroler_Windy::getPozycja_drzwi() const {
    return pozycja_drzwi;
}

void Kontroler_Windy::setPozycja_drzwi(double p_pozycja_drzwi) {
    pozycja_drzwi = p_pozycja_drzwi;
}

double Kontroler_Windy::getPozycja_max_drzwi() const {
    return pozycja_max_drzwi;
}

void Kontroler_Windy::setPozycja_max_drzwi(double p_pozycja_max_drzwi) {
    pozycja_max_drzwi = p_pozycja_max_drzwi;
}

int Kontroler_Windy::getPozycja_max_winda() const {
    return pozycja_max_winda;
}

void Kontroler_Windy::setPozycja_max_winda(int p_pozycja_max_winda) {
    pozycja_max_winda = p_pozycja_max_winda;
}

double Kontroler_Windy::getPozycja_min_drzwi() const {
    return pozycja_min_drzwi;
}

void Kontroler_Windy::setPozycja_min_drzwi(double p_pozycja_min_drzwi) {
    pozycja_min_drzwi = p_pozycja_min_drzwi;
}

int Kontroler_Windy::getPozycja_min_winda() const {
    return pozycja_min_winda;
}

void Kontroler_Windy::setPozycja_min_winda(int p_pozycja_min_winda) {
    pozycja_min_winda = p_pozycja_min_winda;
}

void Kontroler_Windy::_clearItsItsCzujnikpieter_Czujnikpieter() {
    NOTIFY_RELATION_CLEARED("itsItsCzujnikpieter_Czujnikpieter");
    itsItsCzujnikpieter_Czujnikpieter = NULL;
}

void Kontroler_Windy::setActiveContext(IOxfActive* theActiveContext, bool activeInstance) {
    OMReactive::setActiveContext(theActiveContext, activeInstance);
    {
        itsDetektor_kolizji.setActiveContext(theActiveContext, false);
        itsPanel.setActiveContext(theActiveContext, false);
        itsPrzycisk.setActiveContext(theActiveContext, false);
        itsSilnikDrzwi.setActiveContext(theActiveContext, false);
        itsWyswietlacz.setActiveContext(theActiveContext, false);
        itsSilnikWindy.setActiveContext(theActiveContext, false);
        itsSilnik_drzwi.setActiveContext(theActiveContext, false);
        itsGlosnik.setActiveContext(theActiveContext, false);
        itsCzujnikpieter.setActiveContext(theActiveContext, false);
    }
}

void Kontroler_Windy::destroy() {
    itsCzujnikpieter.destroy();
    itsDetektor_kolizji.destroy();
    itsGlosnik.destroy();
    itsPanel.destroy();
    itsPrzycisk.destroy();
    itsSilnikDrzwi.destroy();
    itsSilnikWindy.destroy();
    itsSilnik_drzwi.destroy();
    itsWyswietlacz.destroy();
    OMReactive::destroy();
}

Stany Kontroler_Windy::getStan_windy() const {
    return stan_windy;
}

void Kontroler_Windy::setStan_windy(Stany p_stan_windy) {
    stan_windy = p_stan_windy;
}

Czujnikpieter* Kontroler_Windy::getItsCzujnikpieter_1() const {
    return itsCzujnikpieter_1;
}

void Kontroler_Windy::setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter) {
    if(p_Czujnikpieter != NULL)
        {
            p_Czujnikpieter->_setItsKontroler_Windy(this);
        }
    _setItsCzujnikpieter_1(p_Czujnikpieter);
}

void Kontroler_Windy::setItsItsCzujnikpieter_Czujnikpieter(Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter) {
    if(p_itsCzujnikpieter_Czujnikpieter != NULL)
        {
            p_itsCzujnikpieter_Czujnikpieter->_setItsKontroler_Windy(this);
        }
    _setItsItsCzujnikpieter_Czujnikpieter(p_itsCzujnikpieter_Czujnikpieter);
}

int Kontroler_Windy::getI() const {
    return i;
}

void Kontroler_Windy::setI(int p_i) {
    i = p_i;
}

//#[ ignore
#undef OM_RET_TYPE
#define OM_RET_TYPE 
#undef OM_SER_RET
#define OM_SER_RET(val) 
#undef OM_SER_OUT
#define OM_SER_OUT 
//#]


void Kontroler_Windy::Drzwi_zamkniete() {
    NOTIFY_TRIGGERED_OPERATION(Drzwi_zamkniete, Drzwi_zamkniete(), 0, Default_Kontroler_Windy_Drzwi_zamkniete_SERIALIZE);
    Drzwi_zamkniete_Kontroler_Windy_Event triggerEvent;
    handleTrigger(&triggerEvent);
    OM_RETURN_VOID;
}

void Kontroler_Windy::__setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter) {
    itsCzujnikpieter_1 = p_Czujnikpieter;
    if(p_Czujnikpieter != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsCzujnikpieter_1", p_Czujnikpieter, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsCzujnikpieter_1");
        }
}

void Kontroler_Windy::_setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter) {
    if(itsCzujnikpieter_1 != NULL)
        {
            itsCzujnikpieter_1->__setItsKontroler_Windy(NULL);
        }
    __setItsCzujnikpieter_1(p_Czujnikpieter);
}

void Kontroler_Windy::_clearItsCzujnikpieter_1() {
    NOTIFY_RELATION_CLEARED("itsCzujnikpieter_1");
    itsCzujnikpieter_1 = NULL;
}

void Kontroler_Windy::__setItsItsCzujnikpieter_Czujnikpieter(Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter) {
    itsItsCzujnikpieter_Czujnikpieter = p_itsCzujnikpieter_Czujnikpieter;
    if(p_itsCzujnikpieter_Czujnikpieter != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsItsCzujnikpieter_Czujnikpieter", p_itsCzujnikpieter_Czujnikpieter, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsItsCzujnikpieter_Czujnikpieter");
        }
}

void Kontroler_Windy::_setItsItsCzujnikpieter_Czujnikpieter(Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter) {
    if(itsItsCzujnikpieter_Czujnikpieter != NULL)
        {
            itsItsCzujnikpieter_Czujnikpieter->__setItsKontroler_Windy(NULL);
        }
    __setItsItsCzujnikpieter_Czujnikpieter(p_itsCzujnikpieter_Czujnikpieter);
}

void Kontroler_Windy::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("37");
        NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
        rootState_subState = Oczekiwanie;
        rootState_active = Oczekiwanie;
        //#[ state Oczekiwanie.(Entry) 
        if (i==0){
        std::cout<<std::endl;
        i++;} 
        else if (i==1){
        std::cout<<"Winda w stanie gotowosci"<<std::endl;
        i++;}
        else{ 
        std::cout<<"Oczekiwanie"<<std::endl;}
        
        //#]
        NOTIFY_TRANSITION_TERMINATED("37");
    }
}

IOxfReactive::TakeEventStatus Kontroler_Windy::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Oczekiwanie
        case Oczekiwanie:
        {
            res = Oczekiwanie_handleEvent();
        }
        break;
        // State terminationstate_43
        case terminationstate_43:
        {
            res = Jazda_na_wezwanie_handleEvent();
        }
        break;
        // State sendaction_20
        case sendaction_20:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("29");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_20");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.terminationstate_43");
                    Jazda_na_wezwanie_subState = terminationstate_43;
                    rootState_active = terminationstate_43;
                    NOTIFY_TRANSITION_TERMINATED("29");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_na_wezwanie_handleEvent();
                }
        }
        break;
        // State sendaction_21
        case sendaction_21:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("28");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_21");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.terminationstate_43");
                    Jazda_na_wezwanie_subState = terminationstate_43;
                    rootState_active = terminationstate_43;
                    NOTIFY_TRANSITION_TERMINATED("28");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_na_wezwanie_handleEvent();
                }
        }
        break;
        // State sendaction_19
        case sendaction_19:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("27");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_19");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.terminationstate_43");
                    Jazda_na_wezwanie_subState = terminationstate_43;
                    rootState_active = terminationstate_43;
                    NOTIFY_TRANSITION_TERMINATED("27");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_na_wezwanie_handleEvent();
                }
        }
        break;
        // State sendaction_25
        case sendaction_25:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("30");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro.sendaction_25");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wybrane_pietro.terminationstate_44");
                    Jazda_na_wybrane_pietro_subState = terminationstate_44;
                    rootState_active = terminationstate_44;
                    NOTIFY_TRANSITION_TERMINATED("30");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_na_wybrane_pietro_handleEvent();
                }
        }
        break;
        // State sendaction_27
        case sendaction_27:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("31");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro.sendaction_27");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wybrane_pietro.terminationstate_44");
                    Jazda_na_wybrane_pietro_subState = terminationstate_44;
                    rootState_active = terminationstate_44;
                    NOTIFY_TRANSITION_TERMINATED("31");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Jazda_na_wybrane_pietro_handleEvent();
                }
        }
        break;
        // State terminationstate_44
        case terminationstate_44:
        {
            res = Jazda_na_wybrane_pietro_handleEvent();
        }
        break;
        // State sendaction_42
        case sendaction_42:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("32");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Zamkniecie_drzwi.sendaction_42");
                    NOTIFY_STATE_ENTERED("ROOT.Zamkniecie_drzwi.terminationstate_45");
                    Zamkniecie_drzwi_subState = terminationstate_45;
                    rootState_active = terminationstate_45;
                    NOTIFY_TRANSITION_TERMINATED("32");
                    res = eventConsumed;
                }
            
            if(res == eventNotConsumed)
                {
                    res = Zamkniecie_drzwi_handleEvent();
                }
        }
        break;
        // State terminationstate_45
        case terminationstate_45:
        {
            res = Zamkniecie_drzwi_handleEvent();
        }
        break;
        default:
            break;
    }
    return res;
}

void Kontroler_Windy::Zamkniecie_drzwi_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Zamkniecie_drzwi");
    pushNullTransition();
    rootState_subState = Zamkniecie_drzwi;
    NOTIFY_TRANSITION_STARTED("23");
    NOTIFY_STATE_ENTERED("ROOT.Zamkniecie_drzwi.sendaction_42");
    pushNullTransition();
    Zamkniecie_drzwi_subState = sendaction_42;
    rootState_active = sendaction_42;
    //#[ state Zamkniecie_drzwi.sendaction_42.(Entry) 
    itsSilnik_drzwi_1->GEN(evZamknijDrzwi);
    //#]
    NOTIFY_TRANSITION_TERMINATED("23");
}

IOxfReactive::TakeEventStatus Kontroler_Windy::Zamkniecie_drzwi_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 24 
            if(IS_COMPLETED(Zamkniecie_drzwi)==true)
                {
                    NOTIFY_TRANSITION_STARTED("24");
                    popNullTransition();
                    switch (Zamkniecie_drzwi_subState) {
                        // State sendaction_42
                        case sendaction_42:
                        {
                            popNullTransition();
                            NOTIFY_STATE_EXITED("ROOT.Zamkniecie_drzwi.sendaction_42");
                        }
                        break;
                        // State terminationstate_45
                        case terminationstate_45:
                        {
                            NOTIFY_STATE_EXITED("ROOT.Zamkniecie_drzwi.terminationstate_45");
                        }
                        break;
                        default:
                            break;
                    }
                    Zamkniecie_drzwi_subState = OMNonState;
                    NOTIFY_STATE_EXITED("ROOT.Zamkniecie_drzwi");
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    if (i==0){
                    std::cout<<std::endl;
                    i++;} 
                    else if (i==1){
                    std::cout<<"Winda w stanie gotowosci"<<std::endl;
                    i++;}
                    else{ 
                    std::cout<<"Oczekiwanie"<<std::endl;}
                    
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("24");
                    res = eventConsumed;
                }
        }
    
    return res;
}

IOxfReactive::TakeEventStatus Kontroler_Windy::OczekiwanieTakeevKrok() {
    OMSETPARAMS(evKrok);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    //## transition 17 
    if(stan_windy == WEZWANIE)
        {
            NOTIFY_TRANSITION_STARTED("16");
            NOTIFY_TRANSITION_STARTED("17");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 16 
            aktualne_pietro--;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("17");
            NOTIFY_TRANSITION_TERMINATED("16");
            res = eventConsumed;
        }
    else
        {
            //## transition 18 
            if(stan_windy == WYBOR)
                {
                    NOTIFY_TRANSITION_STARTED("16");
                    NOTIFY_TRANSITION_STARTED("18");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    //#[ transition 16 
                    aktualne_pietro--;
                    //#]
                    Jazda_na_wybrane_pietro_entDef();
                    NOTIFY_TRANSITION_TERMINATED("18");
                    NOTIFY_TRANSITION_TERMINATED("16");
                    res = eventConsumed;
                }
        }
    return res;
}

IOxfReactive::TakeEventStatus Kontroler_Windy::OczekiwanieTakeevStart() {
    OMSETPARAMS(evStart);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    //## transition 20 
    if(stan_windy == WYBOR)
        {
            NOTIFY_TRANSITION_STARTED("19");
            NOTIFY_TRANSITION_STARTED("20");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 19 
            aktualne_pietro++;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("20");
            NOTIFY_TRANSITION_TERMINATED("19");
            res = eventConsumed;
        }
    else
        {
            //## transition 21 
            if(stan_windy == WEZWANIE)
                {
                    NOTIFY_TRANSITION_STARTED("19");
                    NOTIFY_TRANSITION_STARTED("21");
                    NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
                    //#[ transition 19 
                    aktualne_pietro++;
                    //#]
                    Jazda_na_wezwanie_entDef();
                    NOTIFY_TRANSITION_TERMINATED("21");
                    NOTIFY_TRANSITION_TERMINATED("19");
                    res = eventConsumed;
                }
        }
    return res;
}

IOxfReactive::TakeEventStatus Kontroler_Windy::Oczekiwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(ev3_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("9");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 9 
            wybrane_pietro = 3;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("9");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evStart_Default_id))
        {
            res = OczekiwanieTakeevStart();
        }
    else if(IS_EVENT_TYPE_OF(evKrok_Default_id))
        {
            res = OczekiwanieTakeevKrok();
        }
    else if(IS_EVENT_TYPE_OF(ev4_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("10");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 10 
            wybrane_pietro = 4;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("10");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evuruchomienie_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("0");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
            rootState_subState = Oczekiwanie;
            rootState_active = Oczekiwanie;
            //#[ state Oczekiwanie.(Entry) 
            if (i==0){
            std::cout<<std::endl;
            i++;} 
            else if (i==1){
            std::cout<<"Winda w stanie gotowosci"<<std::endl;
            i++;}
            else{ 
            std::cout<<"Oczekiwanie"<<std::endl;}
            
            //#]
            NOTIFY_TRANSITION_TERMINATED("0");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie0_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("1");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 1 
             wezwanie = 0;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("1");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie1_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 2 
             wezwanie = 1;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanieGaraz_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 6 
              wezwanie = (-1);
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("6");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie2_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 3 
             wezwanie = 2;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie3_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 4 
            wezwanie = 3;     
            std::cout<<"Wezwanie"<<wezwanie<<std::endl;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(Drzwi_otwarte_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("22");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            Zamkniecie_drzwi_entDef();
            NOTIFY_TRANSITION_TERMINATED("22");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie4_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 5 
            wezwanie = 4;
            //#]
            Jazda_na_wezwanie_entDef();
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev0Garaz_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("11");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 11 
            wybrane_pietro = -1;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("11");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev0_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("12");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 12 
            wybrane_pietro = 0;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("12");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev1_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("7");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 7 
            wybrane_pietro = 1;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("7");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev2_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("8");
            NOTIFY_STATE_EXITED("ROOT.Oczekiwanie");
            //#[ transition 8 
             wybrane_pietro = 2;
            //#]
            Jazda_na_wybrane_pietro_entDef();
            NOTIFY_TRANSITION_TERMINATED("8");
            res = eventConsumed;
        }
    
    return res;
}

void Kontroler_Windy::Jazda_na_wybrane_pietro_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wybrane_pietro");
    pushNullTransition();
    rootState_subState = Jazda_na_wybrane_pietro;
    Jazda_na_wybrane_pietroEntDef();
}

void Kontroler_Windy::Jazda_na_wybrane_pietroEntDef() {
    //## transition 14 
    if(wybrane_pietro<aktualne_pietro)
        {
            NOTIFY_TRANSITION_STARTED("13");
            NOTIFY_TRANSITION_STARTED("14");
            //#[ transition 13 
            stan_windy = WYBOR;
            //#]
            //#[ transition 14 
            stan_windy_aktualny == DOL;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wybrane_pietro.sendaction_25");
            pushNullTransition();
            Jazda_na_wybrane_pietro_subState = sendaction_25;
            rootState_active = sendaction_25;
            //#[ state Jazda_na_wybrane_pietro.sendaction_25.(Entry) 
            itsSilnikWindy_1->GEN(jedz_w_dol);
            //#]
            NOTIFY_TRANSITION_TERMINATED("14");
            NOTIFY_TRANSITION_TERMINATED("13");
        }
    else
        {
            //## transition 15 
            if(wybrane_pietro > aktualne_pietro)
                {
                    NOTIFY_TRANSITION_STARTED("13");
                    NOTIFY_TRANSITION_STARTED("15");
                    //#[ transition 13 
                    stan_windy = WYBOR;
                    //#]
                    //#[ transition 15 
                    stan_windy_aktualny == GORA;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wybrane_pietro.sendaction_27");
                    pushNullTransition();
                    Jazda_na_wybrane_pietro_subState = sendaction_27;
                    rootState_active = sendaction_27;
                    //#[ state Jazda_na_wybrane_pietro.sendaction_27.(Entry) 
                    itsSilnikWindy_1->GEN(jedz_w_gore);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("15");
                    NOTIFY_TRANSITION_TERMINATED("13");
                }
        }
}

void Kontroler_Windy::Jazda_na_wybrane_pietro_exit() {
    popNullTransition();
    switch (Jazda_na_wybrane_pietro_subState) {
        // State sendaction_25
        case sendaction_25:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro.sendaction_25");
        }
        break;
        // State sendaction_27
        case sendaction_27:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro.sendaction_27");
        }
        break;
        // State terminationstate_44
        case terminationstate_44:
        {
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro.terminationstate_44");
        }
        break;
        default:
            break;
    }
    Jazda_na_wybrane_pietro_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wybrane_pietro");
}

IOxfReactive::TakeEventStatus Kontroler_Windy::Jazda_na_wybrane_pietro_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 25 
            if(IS_COMPLETED(Jazda_na_wybrane_pietro)==true)
                {
                    NOTIFY_TRANSITION_STARTED("25");
                    Jazda_na_wybrane_pietro_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    if (i==0){
                    std::cout<<std::endl;
                    i++;} 
                    else if (i==1){
                    std::cout<<"Winda w stanie gotowosci"<<std::endl;
                    i++;}
                    else{ 
                    std::cout<<"Oczekiwanie"<<std::endl;}
                    
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("25");
                    res = eventConsumed;
                }
        }
    
    return res;
}

void Kontroler_Windy::Jazda_na_wezwanie_entDef() {
    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie");
    pushNullTransition();
    rootState_subState = Jazda_na_wezwanie;
    Jazda_na_wezwanieEntDef();
}

void Kontroler_Windy::Jazda_na_wezwanieEntDef() {
    //## transition 34 
    if(wezwanie < aktualne_pietro)
        {
            NOTIFY_TRANSITION_STARTED("36");
            NOTIFY_TRANSITION_STARTED("34");
            //#[ transition 34 
            stan_windy_aktualny == GORA;
            stan_windy = WEZWANIE;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.sendaction_20");
            pushNullTransition();
            Jazda_na_wezwanie_subState = sendaction_20;
            rootState_active = sendaction_20;
            //#[ state Jazda_na_wezwanie.sendaction_20.(Entry) 
            itsSilnikWindy_1->GEN(jedz_w_gore);
            //#]
            NOTIFY_TRANSITION_TERMINATED("34");
            NOTIFY_TRANSITION_TERMINATED("36");
        }
    else
        {
            //## transition 35 
            if(wezwanie > aktualne_pietro)
                {
                    NOTIFY_TRANSITION_STARTED("36");
                    NOTIFY_TRANSITION_STARTED("35");
                    //#[ transition 35 
                    stan_windy_aktualny == DOL;
                    stan_windy = WEZWANIE;
                    //#]
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.sendaction_19");
                    pushNullTransition();
                    Jazda_na_wezwanie_subState = sendaction_19;
                    rootState_active = sendaction_19;
                    //#[ state Jazda_na_wezwanie.sendaction_19.(Entry) 
                    itsSilnikWindy_1->GEN(jedz_w_dol);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("35");
                    NOTIFY_TRANSITION_TERMINATED("36");
                }
            else
                {
                    NOTIFY_TRANSITION_STARTED("36");
                    NOTIFY_TRANSITION_STARTED("33");
                    NOTIFY_STATE_ENTERED("ROOT.Jazda_na_wezwanie.sendaction_21");
                    pushNullTransition();
                    Jazda_na_wezwanie_subState = sendaction_21;
                    rootState_active = sendaction_21;
                    //#[ state Jazda_na_wezwanie.sendaction_21.(Entry) 
                    itsSilnik_drzwi_1->GEN(evOtrworzDrzwi);
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("33");
                    NOTIFY_TRANSITION_TERMINATED("36");
                }
        }
}

void Kontroler_Windy::Jazda_na_wezwanie_exit() {
    popNullTransition();
    switch (Jazda_na_wezwanie_subState) {
        // State terminationstate_43
        case terminationstate_43:
        {
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.terminationstate_43");
        }
        break;
        // State sendaction_20
        case sendaction_20:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_20");
        }
        break;
        // State sendaction_21
        case sendaction_21:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_21");
        }
        break;
        // State sendaction_19
        case sendaction_19:
        {
            popNullTransition();
            NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie.sendaction_19");
        }
        break;
        default:
            break;
    }
    Jazda_na_wezwanie_subState = OMNonState;
    
    NOTIFY_STATE_EXITED("ROOT.Jazda_na_wezwanie");
}

IOxfReactive::TakeEventStatus Kontroler_Windy::Jazda_na_wezwanie_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(OMNullEventId))
        {
            //## transition 26 
            if(IS_COMPLETED(Jazda_na_wezwanie)==true)
                {
                    NOTIFY_TRANSITION_STARTED("26");
                    Jazda_na_wezwanie_exit();
                    NOTIFY_STATE_ENTERED("ROOT.Oczekiwanie");
                    rootState_subState = Oczekiwanie;
                    rootState_active = Oczekiwanie;
                    //#[ state Oczekiwanie.(Entry) 
                    if (i==0){
                    std::cout<<std::endl;
                    i++;} 
                    else if (i==1){
                    std::cout<<"Winda w stanie gotowosci"<<std::endl;
                    i++;}
                    else{ 
                    std::cout<<"Oczekiwanie"<<std::endl;}
                    
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("26");
                    res = eventConsumed;
                }
        }
    
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedKontroler_Windy::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("aktualne_pietro", x2String(myReal->aktualne_pietro));
    aomsAttributes->addAttribute("wezwanie", x2String(myReal->wezwanie));
    aomsAttributes->addAttribute("wybrane_pietro", x2String(myReal->wybrane_pietro));
    aomsAttributes->addAttribute("pozycja_min_winda", x2String(myReal->pozycja_min_winda));
    aomsAttributes->addAttribute("stan_poprzedni_drzwi", x2String((int)myReal->stan_poprzedni_drzwi));
    aomsAttributes->addAttribute("stan_aktualny_drzwi", x2String((int)myReal->stan_aktualny_drzwi));
    aomsAttributes->addAttribute("stan_windy", x2String((int)myReal->stan_windy));
    aomsAttributes->addAttribute("pozycja", x2String(myReal->pozycja));
    aomsAttributes->addAttribute("pozycja_max_winda", x2String(myReal->pozycja_max_winda));
    aomsAttributes->addAttribute("pozycja_drzwi", x2String(myReal->pozycja_drzwi));
    aomsAttributes->addAttribute("pozycja_min_drzwi", x2String(myReal->pozycja_min_drzwi));
    aomsAttributes->addAttribute("pozycja_max_drzwi", x2String(myReal->pozycja_max_drzwi));
    aomsAttributes->addAttribute("i", x2String(myReal->i));
    aomsAttributes->addAttribute("stan_windy_aktualny", x2String((int)myReal->stan_windy_aktualny));
}

void OMAnimatedKontroler_Windy::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsCzujnikpieter_Czujnikpieter", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsCzujnikpieter_Czujnikpieter);
    aomsRelations->addRelation("itsDetektor_kolizji", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsDetektor_kolizji);
    aomsRelations->addRelation("itsPanel", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPanel);
    aomsRelations->addRelation("itsPrzycisk", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsPrzycisk);
    aomsRelations->addRelation("itsSilnikDrzwi", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSilnikDrzwi);
    aomsRelations->addRelation("itsWyswietlacz", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsWyswietlacz);
    aomsRelations->addRelation("itsSilnikWindy", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSilnikWindy);
    aomsRelations->addRelation("itsSilnik_drzwi", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsSilnik_drzwi);
    aomsRelations->addRelation("itsGlosnik", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsGlosnik);
    aomsRelations->addRelation("itsItsCzujnikpieter_Czujnikpieter", false, true);
    if(myReal->itsItsCzujnikpieter_Czujnikpieter)
        {
            aomsRelations->ADD_ITEM(myReal->itsItsCzujnikpieter_Czujnikpieter);
        }
    aomsRelations->addRelation("itsPanel_1", false, true);
    if(myReal->itsPanel_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsPanel_1);
        }
    aomsRelations->addRelation("itsDetektor_kolizji_1", false, true);
    if(myReal->itsDetektor_kolizji_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsDetektor_kolizji_1);
        }
    aomsRelations->addRelation("itsPrzycisk_1", false, true);
    if(myReal->itsPrzycisk_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsPrzycisk_1);
        }
    aomsRelations->addRelation("itsGlosnik_1", false, true);
    if(myReal->itsGlosnik_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsGlosnik_1);
        }
    aomsRelations->addRelation("itsSilnik_drzwi_1", false, true);
    if(myReal->itsSilnik_drzwi_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSilnik_drzwi_1);
        }
    aomsRelations->addRelation("itsSilnikWindy_1", false, true);
    if(myReal->itsSilnikWindy_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsSilnikWindy_1);
        }
    aomsRelations->addRelation("itsWyswietlacz_1", false, true);
    if(myReal->itsWyswietlacz_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsWyswietlacz_1);
        }
    aomsRelations->addRelation("itsCzujnikpieter", true, true);
    aomsRelations->ADD_ITEM(&myReal->itsCzujnikpieter);
    aomsRelations->addRelation("itsCzujnikpieter_1", false, true);
    if(myReal->itsCzujnikpieter_1)
        {
            aomsRelations->ADD_ITEM(myReal->itsCzujnikpieter_1);
        }
}

void OMAnimatedKontroler_Windy::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Kontroler_Windy::Oczekiwanie:
        {
            Oczekiwanie_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::Jazda_na_wezwanie:
        {
            Jazda_na_wezwanie_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::Jazda_na_wybrane_pietro:
        {
            Jazda_na_wybrane_pietro_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::Zamkniecie_drzwi:
        {
            Zamkniecie_drzwi_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedKontroler_Windy::Zamkniecie_drzwi_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zamkniecie_drzwi");
    switch (myReal->Zamkniecie_drzwi_subState) {
        case Kontroler_Windy::sendaction_42:
        {
            sendaction_42_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::terminationstate_45:
        {
            terminationstate_45_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedKontroler_Windy::terminationstate_45_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zamkniecie_drzwi.terminationstate_45");
}

void OMAnimatedKontroler_Windy::sendaction_42_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Zamkniecie_drzwi.sendaction_42");
}

void OMAnimatedKontroler_Windy::Oczekiwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Oczekiwanie");
}

void OMAnimatedKontroler_Windy::Jazda_na_wybrane_pietro_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wybrane_pietro");
    switch (myReal->Jazda_na_wybrane_pietro_subState) {
        case Kontroler_Windy::sendaction_25:
        {
            sendaction_25_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::sendaction_27:
        {
            sendaction_27_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::terminationstate_44:
        {
            terminationstate_44_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedKontroler_Windy::terminationstate_44_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wybrane_pietro.terminationstate_44");
}

void OMAnimatedKontroler_Windy::sendaction_27_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wybrane_pietro.sendaction_27");
}

void OMAnimatedKontroler_Windy::sendaction_25_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wybrane_pietro.sendaction_25");
}

void OMAnimatedKontroler_Windy::Jazda_na_wezwanie_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wezwanie");
    switch (myReal->Jazda_na_wezwanie_subState) {
        case Kontroler_Windy::terminationstate_43:
        {
            terminationstate_43_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::sendaction_20:
        {
            sendaction_20_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::sendaction_21:
        {
            sendaction_21_serializeStates(aomsState);
        }
        break;
        case Kontroler_Windy::sendaction_19:
        {
            sendaction_19_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedKontroler_Windy::terminationstate_43_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wezwanie.terminationstate_43");
}

void OMAnimatedKontroler_Windy::sendaction_21_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wezwanie.sendaction_21");
}

void OMAnimatedKontroler_Windy::sendaction_20_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wezwanie.sendaction_20");
}

void OMAnimatedKontroler_Windy::sendaction_19_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Jazda_na_wezwanie.sendaction_19");
}

void OMAnimateditsCzujnikpieter_Czujnikpieter_C::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
}
//#]

IMPLEMENT_REACTIVE_META_P(Kontroler_Windy, Default, Default, false, OMAnimatedKontroler_Windy)

IMPLEMENT_META_OBJECT_P(Kontroler_Windy::itsCzujnikpieter_Czujnikpieter, Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C, Default, Default, false, OMAnimateditsCzujnikpieter_Czujnikpieter_C)
#endif // _OMINSTRUMENT

//#[ ignore
Drzwi_zamkniete_Kontroler_Windy_Event::Drzwi_zamkniete_Kontroler_Windy_Event() {
    setId(Drzwi_zamkniete_Kontroler_Windy_Event_id);
}
//#]

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Kontroler_Windy.cpp
*********************************************************************/
