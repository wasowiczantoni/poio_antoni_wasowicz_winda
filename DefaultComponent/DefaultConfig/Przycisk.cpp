/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Przycisk
//!	Generated Date	: Mon, 15, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Przycisk.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Przycisk.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Przycisk_Przycisk_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Przycisk
Przycisk::Przycisk(IOxfActive* theActiveContext) : wezwanie(0), wybrane_pietro(0) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Przycisk, Przycisk(), 0, Default_Przycisk_Przycisk_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Przycisk::~Przycisk() {
    NOTIFY_DESTRUCTOR(~Przycisk, false);
    cleanUpRelations();
}

Kontroler_Windy* Przycisk::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Przycisk::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Przycisk::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Przycisk::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Przycisk::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

int Przycisk::getWezwanie() const {
    return wezwanie;
}

void Przycisk::setWezwanie(int p_wezwanie) {
    wezwanie = p_wezwanie;
}

int Przycisk::getWybrane_pietro() const {
    return wybrane_pietro;
}

void Przycisk::setWybrane_pietro(int p_wybrane_pietro) {
    wybrane_pietro = p_wybrane_pietro;
}

void Przycisk::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Przycisk::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Przycisk::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Przycisk::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Przycisk::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evOK_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    //#[ state Aktywny.(Entry) 
                    std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
                    std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
                    //#]
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            res = Aktywny_handleEvent();
        }
        break;
        
        default:
            break;
    }
    return res;
}

IOxfReactive::TakeEventStatus Przycisk::Aktywny_handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(ev3_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("11");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 11 
            int wybrane_pietro = 3;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("11");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev4_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("12");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 12 
            int wybrane_pietro = 4;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("12");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie0_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("2");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 2 
            int wezwanie = 0;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("2");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie1_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("3");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 3 
            int wezwanie = 1;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("3");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanieGaraz_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("7");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 7 
            int wezwanie = -1;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("7");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie2_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("4");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 4 
            int wezwanie = 2;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("4");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie3_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("5");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 5 
             int wezwanie = 3;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("5");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(evWezwanie4_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("6");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 6 
            int wezwanie = 4;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("6");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev0Garaz_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("13");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 13 
            int wybrane_pietro = -1;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("13");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev0_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("8");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 8 
            int wybrane_pietro = 0;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("8");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(pietro_wybrane_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("14");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            NOTIFY_STATE_ENTERED("ROOT.sendaction_3");
            rootState_subState = sendaction_3;
            rootState_active = sendaction_3;
            //#[ state sendaction_3.(Entry) 
            itsKontroler_Windy->GEN(evWezwanieWindy);
            //#]
            NOTIFY_TRANSITION_TERMINATED("14");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev1_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("9");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 9 
             int wybrane_pietro = 1;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("9");
            res = eventConsumed;
        }
    else if(IS_EVENT_TYPE_OF(ev2_Default_id))
        {
            NOTIFY_TRANSITION_STARTED("10");
            NOTIFY_STATE_EXITED("ROOT.Aktywny");
            //#[ transition 10 
            int wybrane_pietro = 2;
            //#]
            NOTIFY_STATE_ENTERED("ROOT.Aktywny");
            rootState_subState = Aktywny;
            rootState_active = Aktywny;
            //#[ state Aktywny.(Entry) 
            std::cout<<"Wezwane pietro: "<<wezwanie<<std::endl;
            std::cout<<"Wybrane pietro: "<<wybrane_pietro<<std::endl;
            //#]
            NOTIFY_TRANSITION_TERMINATED("10");
            res = eventConsumed;
        }
    
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedPrzycisk::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("wezwanie", x2String(myReal->wezwanie));
    aomsAttributes->addAttribute("wybrane_pietro", x2String(myReal->wybrane_pietro));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedPrzycisk::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedPrzycisk::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Przycisk::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Przycisk::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Przycisk::sendaction_3:
        {
            sendaction_3_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedPrzycisk::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedPrzycisk::sendaction_3_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_3");
}

void OMAnimatedPrzycisk::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Przycisk, Default, false, Modul, OMAnimatedModul, OMAnimatedPrzycisk)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Przycisk.cpp
*********************************************************************/
