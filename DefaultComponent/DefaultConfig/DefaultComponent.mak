
############# Target type (Debug/Release) ##################
############################################################
CPPCompileDebug=-g
CPPCompileRelease=-O
LinkDebug=-g
LinkRelease=-O

CleanupFlagForSimulink=
SIMULINK_CONFIG=False
ifeq ($(SIMULINK_CONFIG),True)
CleanupFlagForSimulink=-DOM_WITH_CLEANUP
endif

ConfigurationCPPCompileSwitches=   $(INCLUDE_QUALIFIER). $(INCLUDE_QUALIFIER)$(OMROOT) $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/oxf $(DEFINE_QUALIFIER)CYGWIN $(INST_FLAGS) $(INCLUDE_PATH) $(INST_INCLUDES) -Wno-write-strings $(CPPCompileDebug) -c  $(CleanupFlagForSimulink)
ConfigurationCCCompileSwitches=$(INCLUDE_PATH) -c 

#########################################
###### Predefined macros ################
RM=/bin/rm -rf
INCLUDE_QUALIFIER=-I
DEFINE_QUALIFIER=-D
CC=g++
LIB_CMD=ar
LINK_CMD=g++
LIB_FLAGS=rvu
LINK_FLAGS= $(LinkDebug)   

#########################################
####### Context macros ##################

FLAGSFILE=
RULESFILE=
OMROOT="C:/ProgramData/IBM/Rational/Rhapsody/8.3.1/Share"
RHPROOT="C:/Program Files/IBM/Rational/Rhapsody/8.3.1"

CPP_EXT=.cpp
H_EXT=.h
OBJ_EXT=.o
EXE_EXT=.exe
LIB_EXT=.a

INSTRUMENTATION=Animation

TIME_MODEL=RealTime

TARGET_TYPE=Executable

TARGET_NAME=DefaultComponent

all : $(TARGET_NAME)$(EXE_EXT) DefaultComponent.mak

TARGET_MAIN=MainDefaultComponent

LIBS=

INCLUDE_PATH= \
  $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/osconfig/Cygwin

ADDITIONAL_OBJS=

OBJS= \
  Przycisk.o \
  Kontroler_Windy.o \
  SilnikWindy.o \
  Czujnikpieter.o \
  Silnik_drzwi.o \
  Detektor_kolizji.o \
  Glosnik.o \
  Wyswietlacz.o \
  Panel.o \
  Modul.o \
  Uzytkownik.o \
  actor_1.o \
  Osoba.o \
  Serwisant.o \
  Otoczenie.o \
  Default.o




#########################################
####### Predefined macros ###############
$(OBJS) : $(INST_LIBS) $(OXF_LIBS)

ifeq ($(INSTRUMENTATION),Animation)

INST_FLAGS=$(DEFINE_QUALIFIER)OMANIMATOR $(DEFINE_QUALIFIER)__USE_W32_SOCKETS 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinaomanim$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),Tracing)

INST_FLAGS=$(DEFINE_QUALIFIER)OMTRACER 
INST_INCLUDES=$(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/aom $(INCLUDE_QUALIFIER)$(OMROOT)/LangCpp/tom
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwintomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinaomtrace$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinoxsiminst$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxfinst$(LIB_EXT) $(OMROOT)/LangCpp/lib/cygwinomcomappl$(LIB_EXT)
SOCK_LIB=-lws2_32

else
ifeq ($(INSTRUMENTATION),None)

INST_FLAGS= 
INST_INCLUDES=
INST_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxsim$(LIB_EXT)
OXF_LIBS=$(OMROOT)/LangCpp/lib/cygwinoxf$(LIB_EXT)
SOCK_LIB=-lws2_32

else
	@echo An invalid Instrumentation $(INSTRUMENTATION) is specified.
	exit
endif
endif
endif

.SUFFIXES: $(CPP_EXT)

#####################################################################
##################### Context dependencies and commands #############






Przycisk.o : Przycisk.cpp Przycisk.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Przycisk.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Przycisk.o Przycisk.cpp




Kontroler_Windy.o : Kontroler_Windy.cpp Kontroler_Windy.h    Default.h Detektor_kolizji.h Panel.h Przycisk.h Silnik_drzwi.h Wyswietlacz.h SilnikWindy.h Czujnikpieter.h Glosnik.h Modul.h 
	@echo Compiling Kontroler_Windy.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Kontroler_Windy.o Kontroler_Windy.cpp




SilnikWindy.o : SilnikWindy.cpp SilnikWindy.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling SilnikWindy.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o SilnikWindy.o SilnikWindy.cpp




Czujnikpieter.o : Czujnikpieter.cpp Czujnikpieter.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Czujnikpieter.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Czujnikpieter.o Czujnikpieter.cpp




Silnik_drzwi.o : Silnik_drzwi.cpp Silnik_drzwi.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Silnik_drzwi.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Silnik_drzwi.o Silnik_drzwi.cpp




Detektor_kolizji.o : Detektor_kolizji.cpp Detektor_kolizji.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Detektor_kolizji.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Detektor_kolizji.o Detektor_kolizji.cpp




Glosnik.o : Glosnik.cpp Glosnik.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Glosnik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Glosnik.o Glosnik.cpp




Wyswietlacz.o : Wyswietlacz.cpp Wyswietlacz.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Wyswietlacz.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Wyswietlacz.o Wyswietlacz.cpp




Panel.o : Panel.cpp Panel.h    Default.h Kontroler_Windy.h Modul.h 
	@echo Compiling Panel.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Panel.o Panel.cpp




Modul.o : Modul.cpp Modul.h    Default.h 
	@echo Compiling Modul.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Modul.o Modul.cpp




Uzytkownik.o : Uzytkownik.cpp Uzytkownik.h    Default.h 
	@echo Compiling Uzytkownik.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Uzytkownik.o Uzytkownik.cpp




actor_1.o : actor_1.cpp actor_1.h    Default.h 
	@echo Compiling actor_1.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o actor_1.o actor_1.cpp




Osoba.o : Osoba.cpp Osoba.h    Default.h Uzytkownik.h 
	@echo Compiling Osoba.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Osoba.o Osoba.cpp




Serwisant.o : Serwisant.cpp Serwisant.h    Default.h Uzytkownik.h 
	@echo Compiling Serwisant.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Serwisant.o Serwisant.cpp




Otoczenie.o : Otoczenie.cpp Otoczenie.h    Default.h 
	@echo Compiling Otoczenie.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Otoczenie.o Otoczenie.cpp




Default.o : Default.cpp Default.h    Przycisk.h Kontroler_Windy.h SilnikWindy.h Czujnikpieter.h Silnik_drzwi.h Detektor_kolizji.h Glosnik.h Wyswietlacz.h Panel.h Modul.h 
	@echo Compiling Default.cpp
	@$(CC) $(ConfigurationCPPCompileSwitches)  -o Default.o Default.cpp







$(TARGET_MAIN)$(OBJ_EXT) : $(TARGET_MAIN)$(CPP_EXT) $(OBJS)
	@echo Compiling $(TARGET_MAIN)$(CPP_EXT)
	@$(CC) $(ConfigurationCPPCompileSwitches) -o  $(TARGET_MAIN)$(OBJ_EXT) $(TARGET_MAIN)$(CPP_EXT)

####################################################################
############## Predefined Instructions #############################
$(TARGET_NAME)$(EXE_EXT): $(OBJS) $(ADDITIONAL_OBJS) $(TARGET_MAIN)$(OBJ_EXT) DefaultComponent.mak
	@echo Linking $(TARGET_NAME)$(EXE_EXT)
	@$(LINK_CMD)  $(TARGET_MAIN)$(OBJ_EXT) $(OBJS) $(ADDITIONAL_OBJS) \
	$(LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(OXF_LIBS) \
	$(INST_LIBS) \
	$(SOCK_LIB) \
	$(LINK_FLAGS) -o $(TARGET_NAME)$(EXE_EXT)

$(TARGET_NAME)$(LIB_EXT) : $(OBJS) $(ADDITIONAL_OBJS) DefaultComponent.mak
	@echo Building library $@
	@$(LIB_CMD) $(LIB_FLAGS) $(TARGET_NAME)$(LIB_EXT) $(OBJS) $(ADDITIONAL_OBJS)



clean:
	@echo Cleanup
	$(RM) Przycisk.o
	$(RM) Kontroler_Windy.o
	$(RM) SilnikWindy.o
	$(RM) Czujnikpieter.o
	$(RM) Silnik_drzwi.o
	$(RM) Detektor_kolizji.o
	$(RM) Glosnik.o
	$(RM) Wyswietlacz.o
	$(RM) Panel.o
	$(RM) Modul.o
	$(RM) Uzytkownik.o
	$(RM) actor_1.o
	$(RM) Osoba.o
	$(RM) Serwisant.o
	$(RM) Otoczenie.o
	$(RM) Default.o
	$(RM) $(TARGET_MAIN)$(OBJ_EXT) $(ADDITIONAL_OBJS)
	$(RM) $(TARGET_NAME)$(LIB_EXT)
	$(RM) $(TARGET_NAME)$(EXE_EXT)

