/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Silnik_drzwi
//!	Generated Date	: Fri, 2, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Silnik_drzwi.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Silnik_drzwi.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Silnik_drzwi_Silnik_drzwi_SERIALIZE OM_NO_OP

#define Default_Silnik_drzwi_Otwieranie_SERIALIZE OM_NO_OP

#define Default_Silnik_drzwi_Zamykanie_SERIALIZE OM_NO_OP

#define Default_Silnik_drzwi_message_0_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Silnik_drzwi
Silnik_drzwi::Silnik_drzwi(IOxfActive* theActiveContext) : kierunek_drzwi(ZAMYKANIE), okres_drzwi(400), predkosc_drzwi(0.5) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Silnik_drzwi, Silnik_drzwi(), 0, Default_Silnik_drzwi_Silnik_drzwi_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Silnik_drzwi::~Silnik_drzwi() {
    NOTIFY_DESTRUCTOR(~Silnik_drzwi, false);
    cleanUpRelations();
    cancelTimeouts();
}

void Silnik_drzwi::Otwieranie() {
    NOTIFY_OPERATION(Otwieranie, Otwieranie(), 0, Default_Silnik_drzwi_Otwieranie_SERIALIZE);
    //#[ operation Otwieranie()
    //#]
}

void Silnik_drzwi::Zamykanie() {
    NOTIFY_OPERATION(Zamykanie, Zamykanie(), 0, Default_Silnik_drzwi_Zamykanie_SERIALIZE);
    //#[ operation Zamykanie()
    //#]
}

void Silnik_drzwi::message_0() {
    NOTIFY_OPERATION(message_0, message_0(), 0, Default_Silnik_drzwi_message_0_SERIALIZE);
    //#[ operation message_0()
    //#]
}

int Silnik_drzwi::getOkres_drzwi() const {
    return okres_drzwi;
}

void Silnik_drzwi::setOkres_drzwi(int p_okres_drzwi) {
    okres_drzwi = p_okres_drzwi;
}

int Silnik_drzwi::getPredkosc_drzwi() const {
    return predkosc_drzwi;
}

void Silnik_drzwi::setPredkosc_drzwi(int p_predkosc_drzwi) {
    predkosc_drzwi = p_predkosc_drzwi;
}

Kontroler_Windy* Silnik_drzwi::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Silnik_drzwi::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Silnik_drzwi::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Silnik_drzwi::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
    rootState_timeout = NULL;
}

void Silnik_drzwi::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

void Silnik_drzwi::cancelTimeouts() {
    cancel(rootState_timeout);
}

bool Silnik_drzwi::cancelTimeout(const IOxfTimeout* arg) {
    bool res = false;
    if(rootState_timeout == arg)
        {
            rootState_timeout = NULL;
            res = true;
        }
    return res;
}

Stany Silnik_drzwi::getKierunek_drzwi() const {
    return kierunek_drzwi;
}

void Silnik_drzwi::setKierunek_drzwi(Stany p_kierunek_drzwi) {
    kierunek_drzwi = p_kierunek_drzwi;
}

void Silnik_drzwi::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Silnik_drzwi::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Silnik_drzwi::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Silnik_drzwi::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("0");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        pushNullTransition();
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("0");
    }
}

IOxfReactive::TakeEventStatus Silnik_drzwi::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("5");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("5");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Close_doors
        case Close_doors:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("3");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Close_doors");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_5");
                            pushNullTransition();
                            rootState_subState = sendaction_5;
                            rootState_active = sendaction_5;
                            //#[ state sendaction_5.(Entry) 
                            itsKontroler_Windy->GEN(evOK);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("3");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Open_doors
        case Open_doors:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("4");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Open_doors");
                            NOTIFY_STATE_ENTERED("ROOT.sendaction_6");
                            pushNullTransition();
                            rootState_subState = sendaction_6;
                            rootState_active = sendaction_6;
                            //#[ state sendaction_6.(Entry) 
                            itsKontroler_Windy->GEN(Drzwi_otwarte);
                            //#]
                            NOTIFY_TRANSITION_TERMINATED("4");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(evZamknijDrzwi_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Waiting");
                    rootState_subState = Waiting;
                    rootState_active = Waiting;
                    rootState_timeout = scheduleTimeout(5000, "ROOT.Waiting");
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(evOtrworzDrzwi_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("2");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Open_doors");
                    rootState_subState = Open_doors;
                    rootState_active = Open_doors;
                    //#[ state Open_doors.(Entry) 
                    kierunek_drzwi = OTWIERANIE;
                    std::cout<<"Kierunek drzwii: "<<kierunek_drzwi<<std::endl;
                    //#]
                    rootState_timeout = scheduleTimeout(2000, "ROOT.Open_doors");
                    NOTIFY_TRANSITION_TERMINATED("2");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_5
        case sendaction_5:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("7");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_5");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("7");
                    res = eventConsumed;
                }
            
        }
        break;
        // State sendaction_6
        case sendaction_6:
        {
            if(IS_EVENT_TYPE_OF(OMNullEventId))
                {
                    NOTIFY_TRANSITION_STARTED("8");
                    popNullTransition();
                    NOTIFY_STATE_EXITED("ROOT.sendaction_6");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("8");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Waiting
        case Waiting:
        {
            if(IS_EVENT_TYPE_OF(OMTimeoutEventId))
                {
                    if(getCurrentEvent() == rootState_timeout)
                        {
                            NOTIFY_TRANSITION_STARTED("6");
                            cancel(rootState_timeout);
                            NOTIFY_STATE_EXITED("ROOT.Waiting");
                            NOTIFY_STATE_ENTERED("ROOT.Close_doors");
                            rootState_subState = Close_doors;
                            rootState_active = Close_doors;
                            //#[ state Close_doors.(Entry) 
                            kierunek_drzwi = ZAMYKANIE;
                            std::cout<<"Kierunek drzwii: "<<kierunek_drzwi<<std::endl;
                            //#]
                            rootState_timeout = scheduleTimeout(2000, "ROOT.Close_doors");
                            NOTIFY_TRANSITION_TERMINATED("6");
                            res = eventConsumed;
                        }
                }
            
        }
        break;
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedSilnik_drzwi::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    aomsAttributes->addAttribute("kierunek_drzwi", x2String((int)myReal->kierunek_drzwi));
    aomsAttributes->addAttribute("predkosc_drzwi", x2String(myReal->predkosc_drzwi));
    aomsAttributes->addAttribute("okres_drzwi", x2String(myReal->okres_drzwi));
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedSilnik_drzwi::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedSilnik_drzwi::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Silnik_drzwi::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::Close_doors:
        {
            Close_doors_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::Open_doors:
        {
            Open_doors_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::sendaction_5:
        {
            sendaction_5_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::sendaction_6:
        {
            sendaction_6_serializeStates(aomsState);
        }
        break;
        case Silnik_drzwi::Waiting:
        {
            Waiting_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedSilnik_drzwi::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedSilnik_drzwi::Waiting_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Waiting");
}

void OMAnimatedSilnik_drzwi::sendaction_6_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_6");
}

void OMAnimatedSilnik_drzwi::sendaction_5_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.sendaction_5");
}

void OMAnimatedSilnik_drzwi::Open_doors_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Open_doors");
}

void OMAnimatedSilnik_drzwi::Close_doors_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Close_doors");
}

void OMAnimatedSilnik_drzwi::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Silnik_drzwi, Default, false, Modul, OMAnimatedModul, OMAnimatedSilnik_drzwi)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Silnik_drzwi.cpp
*********************************************************************/
