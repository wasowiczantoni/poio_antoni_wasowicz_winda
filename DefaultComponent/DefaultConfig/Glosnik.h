/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Glosnik
//!	Generated Date	: Mon, 15, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Glosnik.h
*********************************************************************/

#ifndef Glosnik_H
#define Glosnik_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Glosnik
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Glosnik
class Glosnik : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedGlosnik;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Glosnik(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Glosnik();
    
    ////    Operations    ////
    
    //## operation Info_o_pietrze(int)
    void Info_o_pietrze(int pietro);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Zamykanie:
    //## statechart_method
    inline bool Zamykanie_IN() const;
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // Otwieranie:
    //## statechart_method
    inline bool Otwieranie_IN() const;
    
    // Jazda_w_gore:
    //## statechart_method
    inline bool Jazda_w_gore_IN() const;
    
    // Jazda_w_dol:
    //## statechart_method
    inline bool Jazda_w_dol_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Glosnik_Enum {
        OMNonState = 0,
        Zamykanie = 1,
        Wylaczony = 2,
        Otwieranie = 3,
        Jazda_w_gore = 4,
        Jazda_w_dol = 5,
        Aktywny = 6
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedGlosnik : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Glosnik, OMAnimatedGlosnik)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Zamykanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Otwieranie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_w_gore_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_w_dol_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Glosnik::rootState_IN() const {
    return true;
}

inline bool Glosnik::Zamykanie_IN() const {
    return rootState_subState == Zamykanie;
}

inline bool Glosnik::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Glosnik::Otwieranie_IN() const {
    return rootState_subState == Otwieranie;
}

inline bool Glosnik::Jazda_w_gore_IN() const {
    return rootState_subState == Jazda_w_gore;
}

inline bool Glosnik::Jazda_w_dol_IN() const {
    return rootState_subState == Jazda_w_dol;
}

inline bool Glosnik::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Glosnik.h
*********************************************************************/
