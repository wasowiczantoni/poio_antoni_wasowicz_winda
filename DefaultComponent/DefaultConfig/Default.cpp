/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Default
//!	Generated Date	: Wed, 31, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX
//#]

//## auto_generated
#include "Default.h"
//## classInstance itsKontroler_Windy
#include "Kontroler_Windy.h"
//## auto_generated
#include "Czujnikpieter.h"
//## auto_generated
#include "Detektor_kolizji.h"
//## auto_generated
#include "Glosnik.h"
//## auto_generated
#include "Modul.h"
//## auto_generated
#include "Panel.h"
//## auto_generated
#include "Przycisk.h"
//## auto_generated
#include "Silnik_drzwi.h"
//## auto_generated
#include "SilnikWindy.h"
//## auto_generated
#include "Wyswietlacz.h"
//#[ ignore
#define evWezwanieWindy_SERIALIZE OM_NO_OP

#define evWezwanieWindy_UNSERIALIZE OM_NO_OP

#define evWezwanieWindy_CONSTRUCTOR evWezwanieWindy()

#define ev_na_ktore_pietro_jechac_SERIALIZE OM_NO_OP

#define ev_na_ktore_pietro_jechac_UNSERIALIZE OM_NO_OP

#define ev_na_ktore_pietro_jechac_CONSTRUCTOR ev_na_ktore_pietro_jechac()

#define jedz_w_gore_SERIALIZE OM_NO_OP

#define jedz_w_gore_UNSERIALIZE OM_NO_OP

#define jedz_w_gore_CONSTRUCTOR jedz_w_gore()

#define info_ktore_pietro_SERIALIZE OMADD_SER(pietro, x2String((int)myEvent->pietro))

#define info_ktore_pietro_UNSERIALIZE OMADD_UNSER(int, pietro, OMDestructiveString2X)

#define info_ktore_pietro_CONSTRUCTOR info_ktore_pietro(pietro)

#define evZmianaPietraDOWN_SERIALIZE OM_NO_OP

#define evZmianaPietraDOWN_UNSERIALIZE OM_NO_OP

#define evZmianaPietraDOWN_CONSTRUCTOR evZmianaPietraDOWN()

#define jedz_w_dol_SERIALIZE OM_NO_OP

#define jedz_w_dol_UNSERIALIZE OM_NO_OP

#define jedz_w_dol_CONSTRUCTOR jedz_w_dol()

#define eventmessage_0_SERIALIZE OM_NO_OP

#define eventmessage_0_UNSERIALIZE OM_NO_OP

#define eventmessage_0_CONSTRUCTOR eventmessage_0()

#define evOtrworzDrzwi_SERIALIZE OM_NO_OP

#define evOtrworzDrzwi_UNSERIALIZE OM_NO_OP

#define evOtrworzDrzwi_CONSTRUCTOR evOtrworzDrzwi()

#define Drzwi_otwarte_SERIALIZE OM_NO_OP

#define Drzwi_otwarte_UNSERIALIZE OM_NO_OP

#define Drzwi_otwarte_CONSTRUCTOR Drzwi_otwarte()

#define evZamknijDrzwi_SERIALIZE OM_NO_OP

#define evZamknijDrzwi_UNSERIALIZE OM_NO_OP

#define evZamknijDrzwi_CONSTRUCTOR evZamknijDrzwi()

#define evSprawdzKolizje_SERIALIZE OM_NO_OP

#define evSprawdzKolizje_UNSERIALIZE OM_NO_OP

#define evSprawdzKolizje_CONSTRUCTOR evSprawdzKolizje()

#define evKolizja_SERIALIZE OM_NO_OP

#define evKolizja_UNSERIALIZE OM_NO_OP

#define evKolizja_CONSTRUCTOR evKolizja()

#define evWyborPietra_SERIALIZE OM_NO_OP

#define evWyborPietra_UNSERIALIZE OM_NO_OP

#define evWyborPietra_CONSTRUCTOR evWyborPietra()

#define evWlaczenie_Konserwacji_SERIALIZE OM_NO_OP

#define evWlaczenie_Konserwacji_UNSERIALIZE OM_NO_OP

#define evWlaczenie_Konserwacji_CONSTRUCTOR evWlaczenie_Konserwacji()

#define Potrzebna_weryfikacja_SERIALIZE OM_NO_OP

#define Potrzebna_weryfikacja_UNSERIALIZE OM_NO_OP

#define Potrzebna_weryfikacja_CONSTRUCTOR Potrzebna_weryfikacja()

#define podanie_loginu_SERIALIZE OM_NO_OP

#define podanie_loginu_UNSERIALIZE OM_NO_OP

#define podanie_loginu_CONSTRUCTOR podanie_loginu()

#define Sprawdzenie_poprawnosci_SERIALIZE OM_NO_OP

#define Sprawdzenie_poprawnosci_UNSERIALIZE OM_NO_OP

#define Sprawdzenie_poprawnosci_CONSTRUCTOR Sprawdzenie_poprawnosci()

#define login_poprawny_SERIALIZE OM_NO_OP

#define login_poprawny_UNSERIALIZE OM_NO_OP

#define login_poprawny_CONSTRUCTOR login_poprawny()

#define Brak_autoryzacji_SERIALIZE OM_NO_OP

#define Brak_autoryzacji_UNSERIALIZE OM_NO_OP

#define Brak_autoryzacji_CONSTRUCTOR Brak_autoryzacji()

#define wybor_trybu_SERIALIZE OM_NO_OP

#define wybor_trybu_UNSERIALIZE OM_NO_OP

#define wybor_trybu_CONSTRUCTOR wybor_trybu()

#define co_sprawdzic_SERIALIZE OM_NO_OP

#define co_sprawdzic_UNSERIALIZE OM_NO_OP

#define co_sprawdzic_CONSTRUCTOR co_sprawdzic()

#define Sprawdzamy_SERIALIZE OM_NO_OP

#define Sprawdzamy_UNSERIALIZE OM_NO_OP

#define Sprawdzamy_CONSTRUCTOR Sprawdzamy()

#define evAktywuj_SERIALIZE OM_NO_OP

#define evAktywuj_UNSERIALIZE OM_NO_OP

#define evAktywuj_CONSTRUCTOR evAktywuj()

#define evStart_SERIALIZE OMADD_SER(stan, x2String((int)myEvent->stan))

#define evStart_UNSERIALIZE OMADD_UNSER(int, stan, OMDestructiveString2X)

#define evStart_CONSTRUCTOR evStart(stan)

#define evStop_SERIALIZE OM_NO_OP

#define evStop_UNSERIALIZE OM_NO_OP

#define evStop_CONSTRUCTOR evStop()

#define ev_zabl_czuj_odb_SERIALIZE OM_NO_OP

#define ev_zabl_czuj_odb_UNSERIALIZE OM_NO_OP

#define ev_zabl_czuj_odb_CONSTRUCTOR ev_zabl_czuj_odb()

#define evPrzycisk_SERIALIZE OMADD_SER(Pietro, x2String(myEvent->Pietro))

#define evPrzycisk_UNSERIALIZE OMADD_UNSER(int, Pietro, OMDestructiveString2X)

#define evPrzycisk_CONSTRUCTOR evPrzycisk(Pietro)

#define evKrok_SERIALIZE OMADD_SER(krok, x2String(myEvent->krok))

#define evKrok_UNSERIALIZE OMADD_UNSER(double, krok, OMDestructiveString2X)

#define evKrok_CONSTRUCTOR evKrok(krok)

#define evKrok_drzwi_SERIALIZE OMADD_SER(krok_drzwi, x2String(myEvent->krok_drzwi))

#define evKrok_drzwi_UNSERIALIZE OMADD_UNSER(double, krok_drzwi, OMDestructiveString2X)

#define evKrok_drzwi_CONSTRUCTOR evKrok_drzwi(krok_drzwi)

#define info_o_pietrze_SERIALIZE OM_NO_OP

#define info_o_pietrze_UNSERIALIZE OM_NO_OP

#define info_o_pietrze_CONSTRUCTOR info_o_pietrze()

#define evWezwanie0_SERIALIZE OM_NO_OP

#define evWezwanie0_UNSERIALIZE OM_NO_OP

#define evWezwanie0_CONSTRUCTOR evWezwanie0()

#define evWezwanie1_SERIALIZE OM_NO_OP

#define evWezwanie1_UNSERIALIZE OM_NO_OP

#define evWezwanie1_CONSTRUCTOR evWezwanie1()

#define evWezwanie2_SERIALIZE OM_NO_OP

#define evWezwanie2_UNSERIALIZE OM_NO_OP

#define evWezwanie2_CONSTRUCTOR evWezwanie2()

#define evWezwanie3_SERIALIZE OM_NO_OP

#define evWezwanie3_UNSERIALIZE OM_NO_OP

#define evWezwanie3_CONSTRUCTOR evWezwanie3()

#define evWezwanie4_SERIALIZE OM_NO_OP

#define evWezwanie4_UNSERIALIZE OM_NO_OP

#define evWezwanie4_CONSTRUCTOR evWezwanie4()

#define evWezwanieGaraz_SERIALIZE OM_NO_OP

#define evWezwanieGaraz_UNSERIALIZE OM_NO_OP

#define evWezwanieGaraz_CONSTRUCTOR evWezwanieGaraz()

#define ev1_SERIALIZE OM_NO_OP

#define ev1_UNSERIALIZE OM_NO_OP

#define ev1_CONSTRUCTOR ev1()

#define ev2_SERIALIZE OM_NO_OP

#define ev2_UNSERIALIZE OM_NO_OP

#define ev2_CONSTRUCTOR ev2()

#define ev3_SERIALIZE OM_NO_OP

#define ev3_UNSERIALIZE OM_NO_OP

#define ev3_CONSTRUCTOR ev3()

#define ev4_SERIALIZE OM_NO_OP

#define ev4_UNSERIALIZE OM_NO_OP

#define ev4_CONSTRUCTOR ev4()

#define ev0_SERIALIZE OM_NO_OP

#define ev0_UNSERIALIZE OM_NO_OP

#define ev0_CONSTRUCTOR ev0()

#define ev0Garaz_SERIALIZE OM_NO_OP

#define ev0Garaz_UNSERIALIZE OM_NO_OP

#define ev0Garaz_CONSTRUCTOR ev0Garaz()

#define evOK_SERIALIZE OM_NO_OP

#define evOK_UNSERIALIZE OM_NO_OP

#define evOK_CONSTRUCTOR evOK()

#define evpozycja_koncowa_SERIALIZE OM_NO_OP

#define evpozycja_koncowa_UNSERIALIZE OM_NO_OP

#define evpozycja_koncowa_CONSTRUCTOR evpozycja_koncowa()

#define pietro_wybrane_SERIALIZE OM_NO_OP

#define pietro_wybrane_UNSERIALIZE OM_NO_OP

#define pietro_wybrane_CONSTRUCTOR pietro_wybrane()

#define evS_SERIALIZE OM_NO_OP

#define evS_UNSERIALIZE OM_NO_OP

#define evS_CONSTRUCTOR evS()

#define evuruchomienie_SERIALIZE OM_NO_OP

#define evuruchomienie_UNSERIALIZE OM_NO_OP

#define evuruchomienie_CONSTRUCTOR evuruchomienie()

#define evZmianaPietraUP_SERIALIZE OM_NO_OP

#define evZmianaPietraUP_UNSERIALIZE OM_NO_OP

#define evZmianaPietraUP_CONSTRUCTOR evZmianaPietraUP()
//#]

//## package Default


//## classInstance itsKontroler_Windy
Kontroler_Windy itsKontroler_Windy;

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */);

static void RenameGlobalInstances();

IMPLEMENT_META_PACKAGE(Default, Default)
#endif // _OMINSTRUMENT

void Default_initRelations() {
    {
        {
            itsKontroler_Windy.setShouldDelete(false);
        }
    }
    
    #ifdef _OMINSTRUMENT
    RenameGlobalInstances();
    #endif // _OMINSTRUMENT
}

bool Default_startBehavior() {
    bool done = true;
    done &= itsKontroler_Windy.startBehavior();
    return done;
}

#ifdef _OMINSTRUMENT
static void serializeGlobalVars(AOMSAttributes* /* aomsAttributes */) {
}

static void RenameGlobalInstances() {
    OM_SET_INSTANCE_NAME(&itsKontroler_Windy, Kontroler_Windy, "itsKontroler_Windy", AOMNoMultiplicity);
}
#endif // _OMINSTRUMENT

//#[ ignore
Default_OMInitializer::Default_OMInitializer() {
    Default_initRelations();
    Default_startBehavior();
}

Default_OMInitializer::~Default_OMInitializer() {
}
//#]

//## event evWezwanieWindy()
evWezwanieWindy::evWezwanieWindy() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanieWindy)
    setId(evWezwanieWindy_Default_id);
}

bool evWezwanieWindy::isTypeOf(const short id) const {
    return (evWezwanieWindy_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanieWindy, Default, Default, evWezwanieWindy())

//## event ev_na_ktore_pietro_jechac()
ev_na_ktore_pietro_jechac::ev_na_ktore_pietro_jechac() {
    NOTIFY_EVENT_CONSTRUCTOR(ev_na_ktore_pietro_jechac)
    setId(ev_na_ktore_pietro_jechac_Default_id);
}

bool ev_na_ktore_pietro_jechac::isTypeOf(const short id) const {
    return (ev_na_ktore_pietro_jechac_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev_na_ktore_pietro_jechac, Default, Default, ev_na_ktore_pietro_jechac())

//## event jedz_w_gore()
jedz_w_gore::jedz_w_gore() {
    NOTIFY_EVENT_CONSTRUCTOR(jedz_w_gore)
    setId(jedz_w_gore_Default_id);
}

bool jedz_w_gore::isTypeOf(const short id) const {
    return (jedz_w_gore_Default_id==id);
}

IMPLEMENT_META_EVENT_P(jedz_w_gore, Default, Default, jedz_w_gore())

//## event info_ktore_pietro(PIETRO)
//#[ ignore
info_ktore_pietro::info_ktore_pietro(int p_pietro) : pietro((PIETRO)p_pietro) {
    NOTIFY_EVENT_CONSTRUCTOR(info_ktore_pietro)
    setId(info_ktore_pietro_Default_id);
}
//#]

info_ktore_pietro::info_ktore_pietro() {
    NOTIFY_EVENT_CONSTRUCTOR(info_ktore_pietro)
    setId(info_ktore_pietro_Default_id);
}

info_ktore_pietro::info_ktore_pietro(PIETRO p_pietro) : pietro(p_pietro) {
    NOTIFY_EVENT_CONSTRUCTOR(info_ktore_pietro)
    setId(info_ktore_pietro_Default_id);
}

bool info_ktore_pietro::isTypeOf(const short id) const {
    return (info_ktore_pietro_Default_id==id);
}

IMPLEMENT_META_EVENT_P(info_ktore_pietro, Default, Default, info_ktore_pietro(PIETRO))

//## event evZmianaPietraDOWN()
evZmianaPietraDOWN::evZmianaPietraDOWN() {
    NOTIFY_EVENT_CONSTRUCTOR(evZmianaPietraDOWN)
    setId(evZmianaPietraDOWN_Default_id);
}

bool evZmianaPietraDOWN::isTypeOf(const short id) const {
    return (evZmianaPietraDOWN_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZmianaPietraDOWN, Default, Default, evZmianaPietraDOWN())

//## event jedz_w_dol()
jedz_w_dol::jedz_w_dol() {
    NOTIFY_EVENT_CONSTRUCTOR(jedz_w_dol)
    setId(jedz_w_dol_Default_id);
}

bool jedz_w_dol::isTypeOf(const short id) const {
    return (jedz_w_dol_Default_id==id);
}

IMPLEMENT_META_EVENT_P(jedz_w_dol, Default, Default, jedz_w_dol())

//## event eventmessage_0()
eventmessage_0::eventmessage_0() {
    NOTIFY_EVENT_CONSTRUCTOR(eventmessage_0)
    setId(eventmessage_0_Default_id);
}

bool eventmessage_0::isTypeOf(const short id) const {
    return (eventmessage_0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(eventmessage_0, Default, Default, eventmessage_0())

//## event evOtrworzDrzwi()
evOtrworzDrzwi::evOtrworzDrzwi() {
    NOTIFY_EVENT_CONSTRUCTOR(evOtrworzDrzwi)
    setId(evOtrworzDrzwi_Default_id);
}

bool evOtrworzDrzwi::isTypeOf(const short id) const {
    return (evOtrworzDrzwi_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOtrworzDrzwi, Default, Default, evOtrworzDrzwi())

//## event Drzwi_otwarte()
Drzwi_otwarte::Drzwi_otwarte() {
    NOTIFY_EVENT_CONSTRUCTOR(Drzwi_otwarte)
    setId(Drzwi_otwarte_Default_id);
}

bool Drzwi_otwarte::isTypeOf(const short id) const {
    return (Drzwi_otwarte_Default_id==id);
}

IMPLEMENT_META_EVENT_P(Drzwi_otwarte, Default, Default, Drzwi_otwarte())

//## event evZamknijDrzwi()
evZamknijDrzwi::evZamknijDrzwi() {
    NOTIFY_EVENT_CONSTRUCTOR(evZamknijDrzwi)
    setId(evZamknijDrzwi_Default_id);
}

bool evZamknijDrzwi::isTypeOf(const short id) const {
    return (evZamknijDrzwi_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZamknijDrzwi, Default, Default, evZamknijDrzwi())

//## event evSprawdzKolizje()
evSprawdzKolizje::evSprawdzKolizje() {
    NOTIFY_EVENT_CONSTRUCTOR(evSprawdzKolizje)
    setId(evSprawdzKolizje_Default_id);
}

bool evSprawdzKolizje::isTypeOf(const short id) const {
    return (evSprawdzKolizje_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evSprawdzKolizje, Default, Default, evSprawdzKolizje())

//## event evKolizja()
evKolizja::evKolizja() {
    NOTIFY_EVENT_CONSTRUCTOR(evKolizja)
    setId(evKolizja_Default_id);
}

bool evKolizja::isTypeOf(const short id) const {
    return (evKolizja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKolizja, Default, Default, evKolizja())

//## event evWyborPietra()
evWyborPietra::evWyborPietra() {
    NOTIFY_EVENT_CONSTRUCTOR(evWyborPietra)
    setId(evWyborPietra_Default_id);
}

bool evWyborPietra::isTypeOf(const short id) const {
    return (evWyborPietra_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWyborPietra, Default, Default, evWyborPietra())

//## event evWlaczenie_Konserwacji()
evWlaczenie_Konserwacji::evWlaczenie_Konserwacji() {
    NOTIFY_EVENT_CONSTRUCTOR(evWlaczenie_Konserwacji)
    setId(evWlaczenie_Konserwacji_Default_id);
}

bool evWlaczenie_Konserwacji::isTypeOf(const short id) const {
    return (evWlaczenie_Konserwacji_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWlaczenie_Konserwacji, Default, Default, evWlaczenie_Konserwacji())

//## event Potrzebna_weryfikacja()
Potrzebna_weryfikacja::Potrzebna_weryfikacja() {
    NOTIFY_EVENT_CONSTRUCTOR(Potrzebna_weryfikacja)
    setId(Potrzebna_weryfikacja_Default_id);
}

bool Potrzebna_weryfikacja::isTypeOf(const short id) const {
    return (Potrzebna_weryfikacja_Default_id==id);
}

IMPLEMENT_META_EVENT_P(Potrzebna_weryfikacja, Default, Default, Potrzebna_weryfikacja())

//## event podanie_loginu()
podanie_loginu::podanie_loginu() {
    NOTIFY_EVENT_CONSTRUCTOR(podanie_loginu)
    setId(podanie_loginu_Default_id);
}

bool podanie_loginu::isTypeOf(const short id) const {
    return (podanie_loginu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(podanie_loginu, Default, Default, podanie_loginu())

//## event Sprawdzenie_poprawnosci()
Sprawdzenie_poprawnosci::Sprawdzenie_poprawnosci() {
    NOTIFY_EVENT_CONSTRUCTOR(Sprawdzenie_poprawnosci)
    setId(Sprawdzenie_poprawnosci_Default_id);
}

bool Sprawdzenie_poprawnosci::isTypeOf(const short id) const {
    return (Sprawdzenie_poprawnosci_Default_id==id);
}

IMPLEMENT_META_EVENT_P(Sprawdzenie_poprawnosci, Default, Default, Sprawdzenie_poprawnosci())

//## event login_poprawny()
login_poprawny::login_poprawny() {
    NOTIFY_EVENT_CONSTRUCTOR(login_poprawny)
    setId(login_poprawny_Default_id);
}

bool login_poprawny::isTypeOf(const short id) const {
    return (login_poprawny_Default_id==id);
}

IMPLEMENT_META_EVENT_P(login_poprawny, Default, Default, login_poprawny())

//## event Brak_autoryzacji()
Brak_autoryzacji::Brak_autoryzacji() {
    NOTIFY_EVENT_CONSTRUCTOR(Brak_autoryzacji)
    setId(Brak_autoryzacji_Default_id);
}

bool Brak_autoryzacji::isTypeOf(const short id) const {
    return (Brak_autoryzacji_Default_id==id);
}

IMPLEMENT_META_EVENT_P(Brak_autoryzacji, Default, Default, Brak_autoryzacji())

//## event wybor_trybu()
wybor_trybu::wybor_trybu() {
    NOTIFY_EVENT_CONSTRUCTOR(wybor_trybu)
    setId(wybor_trybu_Default_id);
}

bool wybor_trybu::isTypeOf(const short id) const {
    return (wybor_trybu_Default_id==id);
}

IMPLEMENT_META_EVENT_P(wybor_trybu, Default, Default, wybor_trybu())

//## event co_sprawdzic()
co_sprawdzic::co_sprawdzic() {
    NOTIFY_EVENT_CONSTRUCTOR(co_sprawdzic)
    setId(co_sprawdzic_Default_id);
}

bool co_sprawdzic::isTypeOf(const short id) const {
    return (co_sprawdzic_Default_id==id);
}

IMPLEMENT_META_EVENT_P(co_sprawdzic, Default, Default, co_sprawdzic())

//## event Sprawdzamy()
Sprawdzamy::Sprawdzamy() {
    NOTIFY_EVENT_CONSTRUCTOR(Sprawdzamy)
    setId(Sprawdzamy_Default_id);
}

bool Sprawdzamy::isTypeOf(const short id) const {
    return (Sprawdzamy_Default_id==id);
}

IMPLEMENT_META_EVENT_P(Sprawdzamy, Default, Default, Sprawdzamy())

//## event evAktywuj()
evAktywuj::evAktywuj() {
    NOTIFY_EVENT_CONSTRUCTOR(evAktywuj)
    setId(evAktywuj_Default_id);
}

bool evAktywuj::isTypeOf(const short id) const {
    return (evAktywuj_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evAktywuj, Default, Default, evAktywuj())

//## event evStart(Stany)
//#[ ignore
evStart::evStart(int p_stan) : stan((Stany)p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}
//#]

evStart::evStart() {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

evStart::evStart(Stany p_stan) : stan(p_stan) {
    NOTIFY_EVENT_CONSTRUCTOR(evStart)
    setId(evStart_Default_id);
}

bool evStart::isTypeOf(const short id) const {
    return (evStart_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStart, Default, Default, evStart(Stany))

//## event evStop()
evStop::evStop() {
    NOTIFY_EVENT_CONSTRUCTOR(evStop)
    setId(evStop_Default_id);
}

bool evStop::isTypeOf(const short id) const {
    return (evStop_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evStop, Default, Default, evStop())

//## event ev_zabl_czuj_odb()
ev_zabl_czuj_odb::ev_zabl_czuj_odb() {
    NOTIFY_EVENT_CONSTRUCTOR(ev_zabl_czuj_odb)
    setId(ev_zabl_czuj_odb_Default_id);
}

bool ev_zabl_czuj_odb::isTypeOf(const short id) const {
    return (ev_zabl_czuj_odb_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev_zabl_czuj_odb, Default, Default, ev_zabl_czuj_odb())

//## event evPrzycisk(int)
evPrzycisk::evPrzycisk() {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzycisk)
    setId(evPrzycisk_Default_id);
}

evPrzycisk::evPrzycisk(int p_Pietro) : Pietro(p_Pietro) {
    NOTIFY_EVENT_CONSTRUCTOR(evPrzycisk)
    setId(evPrzycisk_Default_id);
}

bool evPrzycisk::isTypeOf(const short id) const {
    return (evPrzycisk_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evPrzycisk, Default, Default, evPrzycisk(int))

//## event evKrok(double)
evKrok::evKrok() {
    NOTIFY_EVENT_CONSTRUCTOR(evKrok)
    setId(evKrok_Default_id);
}

evKrok::evKrok(double p_krok) : krok(p_krok) {
    NOTIFY_EVENT_CONSTRUCTOR(evKrok)
    setId(evKrok_Default_id);
}

bool evKrok::isTypeOf(const short id) const {
    return (evKrok_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKrok, Default, Default, evKrok(double))

//## event evKrok_drzwi(double)
evKrok_drzwi::evKrok_drzwi() {
    NOTIFY_EVENT_CONSTRUCTOR(evKrok_drzwi)
    setId(evKrok_drzwi_Default_id);
}

evKrok_drzwi::evKrok_drzwi(double p_krok_drzwi) : krok_drzwi(p_krok_drzwi) {
    NOTIFY_EVENT_CONSTRUCTOR(evKrok_drzwi)
    setId(evKrok_drzwi_Default_id);
}

bool evKrok_drzwi::isTypeOf(const short id) const {
    return (evKrok_drzwi_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evKrok_drzwi, Default, Default, evKrok_drzwi(double))

//## event info_o_pietrze()
info_o_pietrze::info_o_pietrze() {
    NOTIFY_EVENT_CONSTRUCTOR(info_o_pietrze)
    setId(info_o_pietrze_Default_id);
}

bool info_o_pietrze::isTypeOf(const short id) const {
    return (info_o_pietrze_Default_id==id);
}

IMPLEMENT_META_EVENT_P(info_o_pietrze, Default, Default, info_o_pietrze())

//## event evWezwanie0()
evWezwanie0::evWezwanie0() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanie0)
    setId(evWezwanie0_Default_id);
}

bool evWezwanie0::isTypeOf(const short id) const {
    return (evWezwanie0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanie0, Default, Default, evWezwanie0())

//## event evWezwanie1()
evWezwanie1::evWezwanie1() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanie1)
    setId(evWezwanie1_Default_id);
}

bool evWezwanie1::isTypeOf(const short id) const {
    return (evWezwanie1_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanie1, Default, Default, evWezwanie1())

//## event evWezwanie2()
evWezwanie2::evWezwanie2() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanie2)
    setId(evWezwanie2_Default_id);
}

bool evWezwanie2::isTypeOf(const short id) const {
    return (evWezwanie2_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanie2, Default, Default, evWezwanie2())

//## event evWezwanie3()
evWezwanie3::evWezwanie3() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanie3)
    setId(evWezwanie3_Default_id);
}

bool evWezwanie3::isTypeOf(const short id) const {
    return (evWezwanie3_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanie3, Default, Default, evWezwanie3())

//## event evWezwanie4()
evWezwanie4::evWezwanie4() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanie4)
    setId(evWezwanie4_Default_id);
}

bool evWezwanie4::isTypeOf(const short id) const {
    return (evWezwanie4_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanie4, Default, Default, evWezwanie4())

//## event evWezwanieGaraz()
evWezwanieGaraz::evWezwanieGaraz() {
    NOTIFY_EVENT_CONSTRUCTOR(evWezwanieGaraz)
    setId(evWezwanieGaraz_Default_id);
}

bool evWezwanieGaraz::isTypeOf(const short id) const {
    return (evWezwanieGaraz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evWezwanieGaraz, Default, Default, evWezwanieGaraz())

//## event ev1()
ev1::ev1() {
    NOTIFY_EVENT_CONSTRUCTOR(ev1)
    setId(ev1_Default_id);
}

bool ev1::isTypeOf(const short id) const {
    return (ev1_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev1, Default, Default, ev1())

//## event ev2()
ev2::ev2() {
    NOTIFY_EVENT_CONSTRUCTOR(ev2)
    setId(ev2_Default_id);
}

bool ev2::isTypeOf(const short id) const {
    return (ev2_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev2, Default, Default, ev2())

//## event ev3()
ev3::ev3() {
    NOTIFY_EVENT_CONSTRUCTOR(ev3)
    setId(ev3_Default_id);
}

bool ev3::isTypeOf(const short id) const {
    return (ev3_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev3, Default, Default, ev3())

//## event ev4()
ev4::ev4() {
    NOTIFY_EVENT_CONSTRUCTOR(ev4)
    setId(ev4_Default_id);
}

bool ev4::isTypeOf(const short id) const {
    return (ev4_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev4, Default, Default, ev4())

//## event ev0()
ev0::ev0() {
    NOTIFY_EVENT_CONSTRUCTOR(ev0)
    setId(ev0_Default_id);
}

bool ev0::isTypeOf(const short id) const {
    return (ev0_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev0, Default, Default, ev0())

//## event ev0Garaz()
ev0Garaz::ev0Garaz() {
    NOTIFY_EVENT_CONSTRUCTOR(ev0Garaz)
    setId(ev0Garaz_Default_id);
}

bool ev0Garaz::isTypeOf(const short id) const {
    return (ev0Garaz_Default_id==id);
}

IMPLEMENT_META_EVENT_P(ev0Garaz, Default, Default, ev0Garaz())

//## event evOK()
evOK::evOK() {
    NOTIFY_EVENT_CONSTRUCTOR(evOK)
    setId(evOK_Default_id);
}

bool evOK::isTypeOf(const short id) const {
    return (evOK_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evOK, Default, Default, evOK())

//## event evpozycja_koncowa()
evpozycja_koncowa::evpozycja_koncowa() {
    NOTIFY_EVENT_CONSTRUCTOR(evpozycja_koncowa)
    setId(evpozycja_koncowa_Default_id);
}

bool evpozycja_koncowa::isTypeOf(const short id) const {
    return (evpozycja_koncowa_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evpozycja_koncowa, Default, Default, evpozycja_koncowa())

//## event pietro_wybrane()
pietro_wybrane::pietro_wybrane() {
    NOTIFY_EVENT_CONSTRUCTOR(pietro_wybrane)
    setId(pietro_wybrane_Default_id);
}

bool pietro_wybrane::isTypeOf(const short id) const {
    return (pietro_wybrane_Default_id==id);
}

IMPLEMENT_META_EVENT_P(pietro_wybrane, Default, Default, pietro_wybrane())

//## event evS()
evS::evS() {
    NOTIFY_EVENT_CONSTRUCTOR(evS)
    setId(evS_Default_id);
}

bool evS::isTypeOf(const short id) const {
    return (evS_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evS, Default, Default, evS())

//## event evuruchomienie()
evuruchomienie::evuruchomienie() {
    NOTIFY_EVENT_CONSTRUCTOR(evuruchomienie)
    setId(evuruchomienie_Default_id);
}

bool evuruchomienie::isTypeOf(const short id) const {
    return (evuruchomienie_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evuruchomienie, Default, Default, evuruchomienie())

//## event evZmianaPietraUP()
evZmianaPietraUP::evZmianaPietraUP() {
    NOTIFY_EVENT_CONSTRUCTOR(evZmianaPietraUP)
    setId(evZmianaPietraUP_Default_id);
}

bool evZmianaPietraUP::isTypeOf(const short id) const {
    return (evZmianaPietraUP_Default_id==id);
}

IMPLEMENT_META_EVENT_P(evZmianaPietraUP, Default, Default, evZmianaPietraUP())

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Default.cpp
*********************************************************************/
