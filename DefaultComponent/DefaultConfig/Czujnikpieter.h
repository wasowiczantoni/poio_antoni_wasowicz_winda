/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Czujnikpieter
//!	Generated Date	: Wed, 31, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Czujnikpieter.h
*********************************************************************/

#ifndef Czujnikpieter_H
#define Czujnikpieter_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## class Czujnikpieter
#include "Modul.h"
//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Czujnikpieter
class Czujnikpieter : public OMReactive, public Modul {
    ////    Friends    ////
    
public :

#ifdef _OMINSTRUMENT
    friend class OMAnimatedCzujnikpieter;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Czujnikpieter(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Czujnikpieter();
    
    ////    Additional operations    ////
    
    //## auto_generated
    Kontroler_Windy* getItsKontroler_Windy() const;
    
    //## auto_generated
    void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    Stany getStan_windy_aktualny() const;
    
    //## auto_generated
    void setStan_windy_aktualny(Stany p_stan_windy_aktualny);
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    unsigned int getAktualne_pietro() const;
    
    //## auto_generated
    void setAktualne_pietro(unsigned int p_aktualne_pietro);
    
    //## auto_generated
    int getKrok_drzwi() const;
    
    //## auto_generated
    void setKrok_drzwi(int p_krok_drzwi);
    
    ////    Attributes    ////

protected :

    unsigned int aktualne_pietro;		//## attribute aktualne_pietro
    
    int krok_drzwi;		//## attribute krok_drzwi
    
    Stany stan_windy_aktualny;		//## attribute stan_windy_aktualny
    
    ////    Relations and components    ////
    
    Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    //## auto_generated
    void _clearItsKontroler_Windy();
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Wylaczony:
    //## statechart_method
    inline bool Wylaczony_IN() const;
    
    // sendaction_5:
    //## statechart_method
    inline bool sendaction_5_IN() const;
    
    // sendaction_4:
    //## statechart_method
    inline bool sendaction_4_IN() const;
    
    // Jazda_w_gore:
    //## statechart_method
    inline bool Jazda_w_gore_IN() const;
    
    // Jazda_w_dol:
    //## statechart_method
    inline bool Jazda_w_dol_IN() const;
    
    // Aktywny:
    //## statechart_method
    inline bool Aktywny_IN() const;
    
    ////    Framework    ////

protected :

//#[ ignore
    enum Czujnikpieter_Enum {
        OMNonState = 0,
        Wylaczony = 1,
        sendaction_5 = 2,
        sendaction_4 = 3,
        Jazda_w_gore = 4,
        Jazda_w_dol = 5,
        Aktywny = 6
    };
    
    int rootState_subState;
    
    int rootState_active;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedCzujnikpieter : public OMAnimatedModul {
    DECLARE_REACTIVE_META(Czujnikpieter, OMAnimatedCzujnikpieter)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Wylaczony_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_5_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_4_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_w_gore_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_w_dol_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Aktywny_serializeStates(AOMSState* aomsState) const;
};
//#]
#endif // _OMINSTRUMENT

inline bool Czujnikpieter::rootState_IN() const {
    return true;
}

inline bool Czujnikpieter::Wylaczony_IN() const {
    return rootState_subState == Wylaczony;
}

inline bool Czujnikpieter::sendaction_5_IN() const {
    return rootState_subState == sendaction_5;
}

inline bool Czujnikpieter::sendaction_4_IN() const {
    return rootState_subState == sendaction_4;
}

inline bool Czujnikpieter::Jazda_w_gore_IN() const {
    return rootState_subState == Jazda_w_gore;
}

inline bool Czujnikpieter::Jazda_w_dol_IN() const {
    return rootState_subState == Jazda_w_dol;
}

inline bool Czujnikpieter::Aktywny_IN() const {
    return rootState_subState == Aktywny;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Czujnikpieter.h
*********************************************************************/
