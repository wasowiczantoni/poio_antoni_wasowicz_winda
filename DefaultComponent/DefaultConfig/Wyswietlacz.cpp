/********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Wyswietlacz
//!	Generated Date	: Mon, 15, Aug 2022  
	File Path	: DefaultComponent/DefaultConfig/Wyswietlacz.cpp
*********************************************************************/

//#[ ignore
#define NAMESPACE_PREFIX

#define _OMSTATECHART_ANIMATED
//#]

//## auto_generated
#include "Wyswietlacz.h"
//## link itsKontroler_Windy
#include "Kontroler_Windy.h"
//#[ ignore
#define Default_Wyswietlacz_Wyswietlacz_SERIALIZE OM_NO_OP
//#]

//## package Default

//## class Wyswietlacz
Wyswietlacz::Wyswietlacz(IOxfActive* theActiveContext) {
    NOTIFY_REACTIVE_CONSTRUCTOR(Wyswietlacz, Wyswietlacz(), 0, Default_Wyswietlacz_Wyswietlacz_SERIALIZE);
    setActiveContext(theActiveContext, false);
    itsKontroler_Windy = NULL;
    initStatechart();
}

Wyswietlacz::~Wyswietlacz() {
    NOTIFY_DESTRUCTOR(~Wyswietlacz, false);
    cleanUpRelations();
}

Kontroler_Windy* Wyswietlacz::getItsKontroler_Windy() const {
    return itsKontroler_Windy;
}

void Wyswietlacz::setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    _setItsKontroler_Windy(p_Kontroler_Windy);
}

bool Wyswietlacz::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Wyswietlacz::initStatechart() {
    rootState_subState = OMNonState;
    rootState_active = OMNonState;
}

void Wyswietlacz::cleanUpRelations() {
    if(itsKontroler_Windy != NULL)
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
            itsKontroler_Windy = NULL;
        }
}

void Wyswietlacz::__setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    itsKontroler_Windy = p_Kontroler_Windy;
    if(p_Kontroler_Windy != NULL)
        {
            NOTIFY_RELATION_ITEM_ADDED("itsKontroler_Windy", p_Kontroler_Windy, false, true);
        }
    else
        {
            NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
        }
}

void Wyswietlacz::_setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy) {
    __setItsKontroler_Windy(p_Kontroler_Windy);
}

void Wyswietlacz::_clearItsKontroler_Windy() {
    NOTIFY_RELATION_CLEARED("itsKontroler_Windy");
    itsKontroler_Windy = NULL;
}

void Wyswietlacz::rootState_entDef() {
    {
        NOTIFY_STATE_ENTERED("ROOT");
        NOTIFY_TRANSITION_STARTED("2");
        NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
        rootState_subState = Wylaczony;
        rootState_active = Wylaczony;
        NOTIFY_TRANSITION_TERMINATED("2");
    }
}

IOxfReactive::TakeEventStatus Wyswietlacz::rootState_processEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    switch (rootState_active) {
        // State Wylaczony
        case Wylaczony:
        {
            if(IS_EVENT_TYPE_OF(evStart_Default_id))
                {
                    OMSETPARAMS(evStart);
                    NOTIFY_TRANSITION_STARTED("0");
                    NOTIFY_STATE_EXITED("ROOT.Wylaczony");
                    NOTIFY_STATE_ENTERED("ROOT.Aktywny");
                    rootState_subState = Aktywny;
                    rootState_active = Aktywny;
                    NOTIFY_TRANSITION_TERMINATED("0");
                    res = eventConsumed;
                }
            
        }
        break;
        // State Aktywny
        case Aktywny:
        {
            if(IS_EVENT_TYPE_OF(evStop_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("1");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.Wylaczony");
                    rootState_subState = Wylaczony;
                    rootState_active = Wylaczony;
                    NOTIFY_TRANSITION_TERMINATED("1");
                    res = eventConsumed;
                }
            else if(IS_EVENT_TYPE_OF(info_o_pietrze_Default_id))
                {
                    NOTIFY_TRANSITION_STARTED("3");
                    NOTIFY_STATE_EXITED("ROOT.Aktywny");
                    NOTIFY_STATE_ENTERED("ROOT.info_pietro");
                    rootState_subState = info_pietro;
                    rootState_active = info_pietro;
                    NOTIFY_TRANSITION_TERMINATED("3");
                    res = eventConsumed;
                }
            
        }
        break;
        
        default:
            break;
    }
    return res;
}

#ifdef _OMINSTRUMENT
//#[ ignore
void OMAnimatedWyswietlacz::serializeAttributes(AOMSAttributes* aomsAttributes) const {
    OMAnimatedModul::serializeAttributes(aomsAttributes);
}

void OMAnimatedWyswietlacz::serializeRelations(AOMSRelations* aomsRelations) const {
    aomsRelations->addRelation("itsKontroler_Windy", false, true);
    if(myReal->itsKontroler_Windy)
        {
            aomsRelations->ADD_ITEM(myReal->itsKontroler_Windy);
        }
    OMAnimatedModul::serializeRelations(aomsRelations);
}

void OMAnimatedWyswietlacz::rootState_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT");
    switch (myReal->rootState_subState) {
        case Wyswietlacz::Wylaczony:
        {
            Wylaczony_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::Aktywny:
        {
            Aktywny_serializeStates(aomsState);
        }
        break;
        case Wyswietlacz::info_pietro:
        {
            info_pietro_serializeStates(aomsState);
        }
        break;
        default:
            break;
    }
}

void OMAnimatedWyswietlacz::Wylaczony_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Wylaczony");
}

void OMAnimatedWyswietlacz::info_pietro_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.info_pietro");
}

void OMAnimatedWyswietlacz::Aktywny_serializeStates(AOMSState* aomsState) const {
    aomsState->addState("ROOT.Aktywny");
}
//#]

IMPLEMENT_REACTIVE_META_S_P(Wyswietlacz, Default, false, Modul, OMAnimatedModul, OMAnimatedWyswietlacz)

OMINIT_SUPERCLASS(Modul, OMAnimatedModul)

OMREGISTER_REACTIVE_CLASS
#endif // _OMINSTRUMENT

/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Wyswietlacz.cpp
*********************************************************************/
