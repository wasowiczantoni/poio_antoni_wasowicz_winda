/*********************************************************************
	Rhapsody	: 8.3.1 
	Login		: student
	Component	: DefaultComponent 
	Configuration 	: DefaultConfig
	Model Element	: Kontroler_Windy
//!	Generated Date	: Fri, 2, Sep 2022  
	File Path	: DefaultComponent/DefaultConfig/Kontroler_Windy.h
*********************************************************************/

#ifndef Kontroler_Windy_H
#define Kontroler_Windy_H

//## auto_generated
#include <oxf/oxf.h>
//## auto_generated
#include <aom/aom.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf/omthread.h>
//## auto_generated
#include <oxf/omreactive.h>
//## auto_generated
#include <oxf/state.h>
//## auto_generated
#include <oxf/event.h>
//## classInstance itsCzujnikpieter
#include "Czujnikpieter.h"
//## classInstance itsDetektor_kolizji
#include "Detektor_kolizji.h"
//## classInstance itsGlosnik
#include "Glosnik.h"
//## classInstance itsPanel
#include "Panel.h"
//## classInstance itsPrzycisk
#include "Przycisk.h"
//## classInstance itsSilnikDrzwi
#include "Silnik_drzwi.h"
//## classInstance itsSilnikWindy
#include "SilnikWindy.h"
//## classInstance itsWyswietlacz
#include "Wyswietlacz.h"
//#[ ignore
#define Drzwi_zamkniete_Kontroler_Windy_Event_id 31000
//#]

//## link itsKontroler_Windy
class Kontroler_Windy;

//## package Default

//## class Kontroler_Windy
class Kontroler_Windy : public OMReactive {
public :

    //## class Kontroler_Windy::itsCzujnikpieter_Czujnikpieter
    class itsCzujnikpieter_Czujnikpieter_C {
        ////    Friends    ////
        
    public :
    
    #ifdef _OMINSTRUMENT
        friend class OMAnimateditsCzujnikpieter_Czujnikpieter_C;
    #endif // _OMINSTRUMENT
    
        ////    Constructors and destructors    ////
        
        //## auto_generated
        itsCzujnikpieter_Czujnikpieter_C();
        
        //## auto_generated
        ~itsCzujnikpieter_Czujnikpieter_C();
        
        ////    Additional operations    ////
        
        //## auto_generated
        Kontroler_Windy* getItsKontroler_Windy() const;
        
        //## auto_generated
        void setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
    
    protected :
    
        //## auto_generated
        void cleanUpRelations();
        
        ////    Relations and components    ////
        
        Kontroler_Windy* itsKontroler_Windy;		//## link itsKontroler_Windy
        
        ////    Framework operations    ////
    
    public :
    
        //## auto_generated
        void __setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
        
        //## auto_generated
        void _setItsKontroler_Windy(Kontroler_Windy* p_Kontroler_Windy);
        
        //## auto_generated
        void _clearItsKontroler_Windy();
    };
    
    ////    Friends    ////
    
#ifdef _OMINSTRUMENT
    friend class OMAnimatedKontroler_Windy;
#endif // _OMINSTRUMENT

    ////    Constructors and destructors    ////
    
    //## auto_generated
    Kontroler_Windy(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Kontroler_Windy();
    
    ////    Operations    ////
    
    //## operation Dekodowanie()
    void Dekodowanie();
    
    //## operation PietroWezwania()
    void PietroWezwania();
    
    //## operation ustawDrzwi()
    void ustawDrzwi();
    
    //## operation ustawStan()
    void ustawStan();
    
    ////    Additional operations    ////
    
    //## auto_generated
    int getAktualne_pietro() const;
    
    //## auto_generated
    void setAktualne_pietro(int p_aktualne_pietro);
    
    //## auto_generated
    Stany getStan_aktualny_drzwi() const;
    
    //## auto_generated
    void setStan_aktualny_drzwi(Stany p_stan_aktualny_drzwi);
    
    //## auto_generated
    Stany getStan_poprzedni_drzwi() const;
    
    //## auto_generated
    void setStan_poprzedni_drzwi(Stany p_stan_poprzedni_drzwi);
    
    //## auto_generated
    Stany getStan_windy_aktualny() const;
    
    //## auto_generated
    void setStan_windy_aktualny(Stany p_stan_windy_aktualny);
    
    //## auto_generated
    int getWezwanie() const;
    
    //## auto_generated
    void setWezwanie(int p_wezwanie);
    
    //## auto_generated
    int getWybrane_pietro() const;
    
    //## auto_generated
    void setWybrane_pietro(int p_wybrane_pietro);
    
    //## auto_generated
    Czujnikpieter* getItsCzujnikpieter() const;
    
    //## auto_generated
    itsCzujnikpieter_Czujnikpieter_C* getItsCzujnikpieter_Czujnikpieter() const;
    
    //## auto_generated
    Detektor_kolizji* getItsDetektor_kolizji() const;
    
    //## auto_generated
    Detektor_kolizji* getItsDetektor_kolizji_1() const;
    
    //## auto_generated
    void setItsDetektor_kolizji_1(Detektor_kolizji* p_Detektor_kolizji);
    
    //## auto_generated
    Glosnik* getItsGlosnik() const;
    
    //## auto_generated
    Glosnik* getItsGlosnik_1() const;
    
    //## auto_generated
    void setItsGlosnik_1(Glosnik* p_Glosnik);
    
    //## auto_generated
    itsCzujnikpieter_Czujnikpieter_C* getItsItsCzujnikpieter_Czujnikpieter() const;
    
    //## auto_generated
    Panel* getItsPanel() const;
    
    //## auto_generated
    Panel* getItsPanel_1() const;
    
    //## auto_generated
    void setItsPanel_1(Panel* p_Panel);
    
    //## auto_generated
    Przycisk* getItsPrzycisk() const;
    
    //## auto_generated
    Przycisk* getItsPrzycisk_1() const;
    
    //## auto_generated
    void setItsPrzycisk_1(Przycisk* p_Przycisk);
    
    //## auto_generated
    Silnik_drzwi* getItsSilnikDrzwi() const;
    
    //## auto_generated
    SilnikWindy* getItsSilnikWindy() const;
    
    //## auto_generated
    SilnikWindy* getItsSilnikWindy_1() const;
    
    //## auto_generated
    void setItsSilnikWindy_1(SilnikWindy* p_SilnikWindy);
    
    //## auto_generated
    Silnik_drzwi* getItsSilnik_drzwi() const;
    
    //## auto_generated
    Silnik_drzwi* getItsSilnik_drzwi_1() const;
    
    //## auto_generated
    void setItsSilnik_drzwi_1(Silnik_drzwi* p_Silnik_drzwi);
    
    //## auto_generated
    Wyswietlacz* getItsWyswietlacz() const;
    
    //## auto_generated
    Wyswietlacz* getItsWyswietlacz_1() const;
    
    //## auto_generated
    void setItsWyswietlacz_1(Wyswietlacz* p_Wyswietlacz);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initRelations();
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();

private :

    //## auto_generated
    int getPozycja() const;
    
    //## auto_generated
    void setPozycja(int p_pozycja);
    
    //## auto_generated
    double getPozycja_drzwi() const;
    
    //## auto_generated
    void setPozycja_drzwi(double p_pozycja_drzwi);
    
    //## auto_generated
    double getPozycja_max_drzwi() const;
    
    //## auto_generated
    void setPozycja_max_drzwi(double p_pozycja_max_drzwi);
    
    //## auto_generated
    int getPozycja_max_winda() const;
    
    //## auto_generated
    void setPozycja_max_winda(int p_pozycja_max_winda);
    
    //## auto_generated
    double getPozycja_min_drzwi() const;
    
    //## auto_generated
    void setPozycja_min_drzwi(double p_pozycja_min_drzwi);
    
    //## auto_generated
    int getPozycja_min_winda() const;
    
    //## auto_generated
    void setPozycja_min_winda(int p_pozycja_min_winda);
    
    ////    Attributes    ////

protected :

    int aktualne_pietro;		//## attribute aktualne_pietro
    
    int pozycja;		//## attribute pozycja
    
    double pozycja_drzwi;		//## attribute pozycja_drzwi
    
    double pozycja_max_drzwi;		//## attribute pozycja_max_drzwi
    
    int pozycja_max_winda;		//## attribute pozycja_max_winda
    
    double pozycja_min_drzwi;		//## attribute pozycja_min_drzwi
    
    int pozycja_min_winda;		//## attribute pozycja_min_winda
    
    Stany stan_aktualny_drzwi;		//## attribute stan_aktualny_drzwi
    
    Stany stan_poprzedni_drzwi;		//## attribute stan_poprzedni_drzwi
    
    Stany stan_windy;		//## attribute stan_windy
    
    int wezwanie;		//## attribute wezwanie
    
    int wybrane_pietro;		//## attribute wybrane_pietro
    
    ////    Relations and components    ////
    
    Detektor_kolizji itsDetektor_kolizji;		//## classInstance itsDetektor_kolizji
    
    Detektor_kolizji* itsDetektor_kolizji_1;		//## link itsDetektor_kolizji_1
    
    Glosnik itsGlosnik;		//## classInstance itsGlosnik
    
    Glosnik* itsGlosnik_1;		//## link itsGlosnik_1
    
    itsCzujnikpieter_Czujnikpieter_C* itsItsCzujnikpieter_Czujnikpieter;		//## link itsItsCzujnikpieter_Czujnikpieter
    
    Panel itsPanel;		//## classInstance itsPanel
    
    Panel* itsPanel_1;		//## link itsPanel_1
    
    Przycisk itsPrzycisk;		//## classInstance itsPrzycisk
    
    Przycisk* itsPrzycisk_1;		//## link itsPrzycisk_1
    
    Silnik_drzwi itsSilnikDrzwi;		//## classInstance itsSilnikDrzwi
    
    SilnikWindy itsSilnikWindy;		//## classInstance itsSilnikWindy
    
    SilnikWindy* itsSilnikWindy_1;		//## link itsSilnikWindy_1
    
    Silnik_drzwi itsSilnik_drzwi;		//## classInstance itsSilnik_drzwi
    
    Silnik_drzwi* itsSilnik_drzwi_1;		//## link itsSilnik_drzwi_1
    
    Wyswietlacz itsWyswietlacz;		//## classInstance itsWyswietlacz
    
    Wyswietlacz* itsWyswietlacz_1;		//## link itsWyswietlacz_1
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void _clearItsItsCzujnikpieter_Czujnikpieter();
    
    //## auto_generated
    void setActiveContext(IOxfActive* theActiveContext, bool activeInstance);
    
    //## auto_generated
    virtual void destroy();
    
    ////    Framework    ////
    
    //## auto_generated
    Stany getStan_windy() const;
    
    //## auto_generated
    void setStan_windy(Stany p_stan_windy);
    
    //## auto_generated
    Czujnikpieter* getItsCzujnikpieter_1() const;
    
    //## auto_generated
    void setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter);
    
    //## auto_generated
    void setItsItsCzujnikpieter_Czujnikpieter(itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter);

private :

    //## auto_generated
    int getI() const;
    
    //## auto_generated
    void setI(int p_i);

public :

    //## TriggeredOperation Drzwi_zamkniete()
    void Drzwi_zamkniete();

protected :

    int i;		//## attribute i
    
    Stany stan_windy_aktualny;		//## attribute stan_windy_aktualny
    
    Czujnikpieter itsCzujnikpieter;		//## classInstance itsCzujnikpieter
    
    Czujnikpieter* itsCzujnikpieter_1;		//## link itsCzujnikpieter_1
    
    itsCzujnikpieter_Czujnikpieter_C itsCzujnikpieter_Czujnikpieter;		//## classInstance itsCzujnikpieter_Czujnikpieter

public :

    //## auto_generated
    void __setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter);
    
    //## auto_generated
    void _setItsCzujnikpieter_1(Czujnikpieter* p_Czujnikpieter);
    
    //## auto_generated
    void _clearItsCzujnikpieter_1();
    
    //## auto_generated
    void __setItsItsCzujnikpieter_Czujnikpieter(itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter);
    
    //## auto_generated
    void _setItsItsCzujnikpieter_Czujnikpieter(itsCzujnikpieter_Czujnikpieter_C* p_itsCzujnikpieter_Czujnikpieter);
    
    // rootState:
    //## statechart_method
    inline bool rootState_IN() const;
    
    //## statechart_method
    virtual void rootState_entDef();
    
    //## statechart_method
    virtual IOxfReactive::TakeEventStatus rootState_processEvent();
    
    // Zamkniecie_drzwi:
    //## statechart_method
    inline bool Zamkniecie_drzwi_IN() const;
    
    //## statechart_method
    inline bool Zamkniecie_drzwi_isCompleted();
    
    //## statechart_method
    void Zamkniecie_drzwi_entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Zamkniecie_drzwi_handleEvent();
    
    // terminationstate_45:
    //## statechart_method
    inline bool terminationstate_45_IN() const;
    
    // sendaction_42:
    //## statechart_method
    inline bool sendaction_42_IN() const;
    
    // Oczekiwanie:
    //## statechart_method
    inline bool Oczekiwanie_IN() const;
    
    //## statechart_method
    IOxfReactive::TakeEventStatus OczekiwanieTakeevKrok();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus OczekiwanieTakeevStart();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Oczekiwanie_handleEvent();
    
    // Jazda_na_wybrane_pietro:
    //## statechart_method
    inline bool Jazda_na_wybrane_pietro_IN() const;
    
    //## statechart_method
    inline bool Jazda_na_wybrane_pietro_isCompleted();
    
    //## statechart_method
    void Jazda_na_wybrane_pietro_entDef();
    
    //## statechart_method
    void Jazda_na_wybrane_pietroEntDef();
    
    //## statechart_method
    void Jazda_na_wybrane_pietro_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Jazda_na_wybrane_pietro_handleEvent();
    
    // terminationstate_44:
    //## statechart_method
    inline bool terminationstate_44_IN() const;
    
    // sendaction_27:
    //## statechart_method
    inline bool sendaction_27_IN() const;
    
    // sendaction_25:
    //## statechart_method
    inline bool sendaction_25_IN() const;
    
    // Jazda_na_wezwanie:
    //## statechart_method
    inline bool Jazda_na_wezwanie_IN() const;
    
    //## statechart_method
    inline bool Jazda_na_wezwanie_isCompleted();
    
    //## statechart_method
    void Jazda_na_wezwanie_entDef();
    
    //## statechart_method
    void Jazda_na_wezwanieEntDef();
    
    //## statechart_method
    void Jazda_na_wezwanie_exit();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus Jazda_na_wezwanie_handleEvent();
    
    // terminationstate_43:
    //## statechart_method
    inline bool terminationstate_43_IN() const;
    
    // sendaction_21:
    //## statechart_method
    inline bool sendaction_21_IN() const;
    
    // sendaction_20:
    //## statechart_method
    inline bool sendaction_20_IN() const;
    
    // sendaction_19:
    //## statechart_method
    inline bool sendaction_19_IN() const;

protected :

//#[ ignore
    enum Kontroler_Windy_Enum {
        OMNonState = 0,
        Zamkniecie_drzwi = 1,
        terminationstate_45 = 2,
        sendaction_42 = 3,
        Oczekiwanie = 4,
        Jazda_na_wybrane_pietro = 5,
        terminationstate_44 = 6,
        sendaction_27 = 7,
        sendaction_25 = 8,
        Jazda_na_wezwanie = 9,
        terminationstate_43 = 10,
        sendaction_21 = 11,
        sendaction_20 = 12,
        sendaction_19 = 13
    };
    
    int rootState_subState;
    
    int rootState_active;
    
    int Zamkniecie_drzwi_subState;
    
    int Jazda_na_wybrane_pietro_subState;
    
    int Jazda_na_wezwanie_subState;
//#]
};

#ifdef _OMINSTRUMENT
//#[ ignore
class OMAnimatedKontroler_Windy : virtual public AOMInstance {
    DECLARE_REACTIVE_META(Kontroler_Windy, OMAnimatedKontroler_Windy)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeAttributes(AOMSAttributes* aomsAttributes) const;
    
    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
    
    //## statechart_method
    void rootState_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Zamkniecie_drzwi_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_45_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_42_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Oczekiwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_na_wybrane_pietro_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_44_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_27_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_25_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void Jazda_na_wezwanie_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void terminationstate_43_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_21_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_20_serializeStates(AOMSState* aomsState) const;
    
    //## statechart_method
    void sendaction_19_serializeStates(AOMSState* aomsState) const;
};

class OMAnimateditsCzujnikpieter_Czujnikpieter_C : virtual public AOMInstance {
    DECLARE_META(Kontroler_Windy::itsCzujnikpieter_Czujnikpieter_C, OMAnimateditsCzujnikpieter_Czujnikpieter_C)
    
    ////    Framework operations    ////
    
public :

    virtual void serializeRelations(AOMSRelations* aomsRelations) const;
};
//#]
#endif // _OMINSTRUMENT

//#[ ignore
class Drzwi_zamkniete_Kontroler_Windy_Event : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    Drzwi_zamkniete_Kontroler_Windy_Event();
};
//#]

inline bool Kontroler_Windy::rootState_IN() const {
    return true;
}

inline bool Kontroler_Windy::Zamkniecie_drzwi_IN() const {
    return rootState_subState == Zamkniecie_drzwi;
}

inline bool Kontroler_Windy::Zamkniecie_drzwi_isCompleted() {
    return ( IS_IN(terminationstate_45) );
}

inline bool Kontroler_Windy::terminationstate_45_IN() const {
    return Zamkniecie_drzwi_subState == terminationstate_45;
}

inline bool Kontroler_Windy::sendaction_42_IN() const {
    return Zamkniecie_drzwi_subState == sendaction_42;
}

inline bool Kontroler_Windy::Oczekiwanie_IN() const {
    return rootState_subState == Oczekiwanie;
}

inline bool Kontroler_Windy::Jazda_na_wybrane_pietro_IN() const {
    return rootState_subState == Jazda_na_wybrane_pietro;
}

inline bool Kontroler_Windy::Jazda_na_wybrane_pietro_isCompleted() {
    return ( IS_IN(terminationstate_44) );
}

inline bool Kontroler_Windy::terminationstate_44_IN() const {
    return Jazda_na_wybrane_pietro_subState == terminationstate_44;
}

inline bool Kontroler_Windy::sendaction_27_IN() const {
    return Jazda_na_wybrane_pietro_subState == sendaction_27;
}

inline bool Kontroler_Windy::sendaction_25_IN() const {
    return Jazda_na_wybrane_pietro_subState == sendaction_25;
}

inline bool Kontroler_Windy::Jazda_na_wezwanie_IN() const {
    return rootState_subState == Jazda_na_wezwanie;
}

inline bool Kontroler_Windy::Jazda_na_wezwanie_isCompleted() {
    return ( IS_IN(terminationstate_43) );
}

inline bool Kontroler_Windy::terminationstate_43_IN() const {
    return Jazda_na_wezwanie_subState == terminationstate_43;
}

inline bool Kontroler_Windy::sendaction_21_IN() const {
    return Jazda_na_wezwanie_subState == sendaction_21;
}

inline bool Kontroler_Windy::sendaction_20_IN() const {
    return Jazda_na_wezwanie_subState == sendaction_20;
}

inline bool Kontroler_Windy::sendaction_19_IN() const {
    return Jazda_na_wezwanie_subState == sendaction_19;
}

#endif
/*********************************************************************
	File Path	: DefaultComponent/DefaultConfig/Kontroler_Windy.h
*********************************************************************/
